<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function json_response($status, $message = '', $data = [], $err_code = ''){
	if ($data == []) {
		$data = new stdClass();
	}
	$return = [];
	$return = [
		'status'   => $status,
		'message'  => $message,
		'list'     => $data,
		'err_code' => $err_code,
	];
	echo json_encode($return);
	die();
}

function get_pages($total, $limit)
{
    $quotient =(double)$total/(double)$limit;
    $int_quotient = (int) $quotient;
    $init_page_count = (double)$int_quotient;
    if ($quotient > $init_page_count) return (int)$init_page_count+1;
    else if($quotient == $init_page_count) return (int)$init_page_count;
}