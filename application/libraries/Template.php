<?php
	defined('BASEPATH') or exit('No access allowed');
	
	class Template {
		private $_ci;
		private $_partials=array();
		private $_bodyLayout="";

		public function __construct(){
			$this->_ci= & get_instance();
		}

		public function buildPage($view,$data=array()){
			foreach ($this->_partials as $key => $value) {
				$data_display[$key]=$this->_ci->load->view($this->_partials[$key]['view'], $data, true);
			}

			$data_display['build_Data']=$this->_ci->load->view($view, NULL, true); //the main body of the page

			$data_display['layout_data']=$this->_bodyLayout['data']; //body content
			$this->_ci->load->view($this->_bodyLayout['view'],$data_display); //set layout
		}

		public function set_layout($view, $data = array()){
			$this->_bodyLayout= array('view' => $view, 'data' => $data);
			return $this;
		}


		public function initPartials($type, $view, $data = array()){
			$this->_partials[$type]= array('view' => 'layout/'.$type.'/'.$view, 'data' => $data );
			return $this;
		}
	}