<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common_model extends CI_Model{
	
	function __construct(){
		parent::__construct();
	}
	
	public function ss_insert($table = '', $data = []){
		$insert = $this->db->insert($table, $data);
		if ($insert) {
			$insert_id = $this->db->insert_id();
			return $insert_id;
		}else{
			return false;
		}
	}

	public function ss_delete($table = '', $data = []){
		$delete = $this->db->delete($table, $data);
		if ($delete) {
			return true;
		}else{
			return false;
		}
	}

	public function ss_update($table = '', $data = [], $param){
		if($table){
			if ( $param ) {
				foreach ($param as $key => $value) {
					if ( is_array($value) ){
						$this->db->where_in($key, $value);
					}else{
						$this->db->where($key, $value);
					}
				}
			}
			return $this->db->update($table, $data);
		}
	}

	public function ss_table_data($table, $fields = [], $where = [],$order_by = [],$paging = [],$search = []){
		$this->db->start_cache();
		// fields
		if(!empty($fields)){
			$this->db->select($fields);
		}else{
			$this->db->select('*');
		}

		// table
		if(isset($table)){
			$this->db->from($table);
		}

		if (!empty($where)) {
			foreach ($where as $key => $val) {
				if (is_array($val)) {
					if(!empty($val)){
						$this->db->where_in($key, $val);
					}
				} else {
					if ($key!="") {
						$this->db->where($key, $val);
					}else{
						$this->db->where($val);
					}
					
				}
			}
		}

		if (!empty($order_by)) {
            if (is_array($order_by)) {
                foreach ($order_by as $o => $b) {
                    if (is_numeric($o)) {
                        $this->db->order_by($b, 'asc');
                    } else {
                        $this->db->order_by($o, $b);
                    }
                }
            } else {
                $this->db->order_by($order_by);
            }
        }

        if (!empty($search)) {
            $like_statements = array();

            foreach ($search as $key => $value) {
                $like_statements[] = "{$key} LIKE '%{$value}%'";
            }
            $like_string = "(" . implode(' OR ', $like_statements) . ")";
            $this->db->where($like_string);
        }

        if (!empty($paging['per_page'])) {
            $this->db->limit($paging['per_page'], $paging['segment']);
        }

		$query = $this->db->get();
		// var_dump($this->db->last_query());
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $query->result_array();
	}

	public function get_requests($fields = [],$param = [],$paging = []){
		$this->db->start_cache();

		if (!empty($fields)) {
            $this->db->select($fields);
        } else {
            $this->db->select('req.user_id,req.property_id,req.id,req.status,req.date,req.receipt,acc.fname,acc.lname,acc.mname,acc.email,acc.contact_number,prop.property_name,prop.property_location,prop.selling_price,prop.sale_type, prop.availability');
        }

		$this->db->from('ss_requests AS req');
		$this->db->join('ss_accounts AS acc','req.user_id = acc.user_id','LEFT');
		$this->db->join('ss_properties AS prop','req.property_id = prop.pid','LEFT');

		if(!empty($param)){
			foreach($param as $i => $v){
				if(!is_array($param[$i])){
					$this->db->where($i, $param[$i]);							
				}else{
					$this->db->where_in($i, $param[$i]);
				}
			}
		}

		if(!empty($paging)){
			$this->db->limit($paging['per_page'], $paging['segment']);
		}

		$this->db->order_by('req.id', 'DESC');

		$query = $this->db->get();
		// var_dump($this->db->last_query());
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $query->result_array();
	}
}// end class Admin_model

?>