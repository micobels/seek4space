<div class="content">
    <div class="container-fluid">
        <div class="row">
        	<div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Accounts List</h4>
                        <p class="category">Here is a list of all Accounts</p>
                    </div>
                    <div class="content table-responsive table-full-width">
                        <table class="table table-striped" id="accounts_table">
                            <thead>
                                <th>Account ID</th>
                            	<th>Name</th>
                            	<th>Email</th>
                            	<th>Mobile</th>
                            	<th>Type</th>
                            	<th>Action</th>
                            </thead>
                            <tbody>
                                <?php foreach ($list as $key => $val): ?>
                                <tr>
                                	<td><?=$val['user_id']?></td>
                                	<td><?=$val['fname'].' '.$val['mname'].' '.$val['lname']?></td>
                                	<td><?=$val['email']?></td>
                                	<td><?=$val['contact_number']?></td>
                                	<td><?php
                                    switch ($val['user_type']) {
                                        case 0:
                                            echo 'Super Admin';
                                            break; 
                                        case 1:
                                            echo 'Buyer';
                                            break; 
                                        case 2:
                                            echo 'Seller';
                                            break; 
                                        default:
                                            echo 'Super Admin';
                                            break;
                                    }
                                    ?></td>
                                	<td>
                                        <button class="btn btn-success btn-icon btn-sm account_details" data-id="<?=$val['user_id']?>"><i class="ti-eye"></i></button>
                                        <button class="btn btn-danger btn-icon btn-sm delete_account" data-id="<?=$val['user_id']?>"><i class="ti-trash"></i></button>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>

                        <?php if($pages > 1): ?>
                        <div class="pagination_area">
                            <div class="controls first_page">
                                <i class="fa fa-angle-double-left"></i>
                            </div>
                            <div class="controls prev_page">
                                <i class="fa fa-angle-left"></i>
                            </div>
                            <div class="current_page">
                                <input type="number" value="1" max="<?=$pages?>">
                                <span style="margin: auto 0 auto 5px;">of </span>
                                <span class="total_page" style="margin: auto 5px;"><?=$pages?></span>
                            </div>
                            <div class="controls next_page">
                                <i class="fa fa-angle-right"></i>
                            </div>
                            <div class="controls last_page">
                                <i class="fa fa-angle-double-right"></i>
                            </div>
                        </div>
                        <?php endif;?>
                    </div>

                    <div id="account_details" style="max-height: 600px;">
                        <table class="table">
                            <tbody></tbody>
                        </table>
                    </div>

                    <div id="success_modal" data-izimodal-title="Success" data-izimodal-subtitle="Account Successfully Deleted!">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>