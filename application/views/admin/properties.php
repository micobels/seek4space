<style type="text/css">
    .card-user .author{
        margin-top: 0;
    }
    .card-user .author .title small{
        color: #105a9c;
    }
    .card-user .content{
        height: 180px;
        min-height: 180px;
    }
    .management_btn{
        padding: 5px 0 10px;
        display: flex;
    }
    .management_btn button{
        width: 100%;
    }
    .other_content{
        width: 100%;
        padding: 20px 0;
    }
    .search_container{
        float: right;
    }
    .search_container input{
        padding: 8px;
        border: solid 1px #d3d3d3;
        border-radius: 35px;
        display: inline-block;
        text-align: center;
    }
    .search_container button{
        padding: 8px;
        border: solid 1px #d3d3d3;
        border-radius: 30px;
        transition: all ease-in-out 0.3s;
    }
    .search_container button:hover{
        background: #3091B2;
        color: #fff;
        border-color: #3091B2;
    }
</style>
<div class="other_content">
    <div class="container-fluid">
        <a href="<?=base_url()?>Admin/Add_property">
            <button class="btn btn-info btn-fill">Add Property</button>
        </a>
        <div class="search_container">
            <input type="text" class="search_prop_input" placeholder="Search Here">
            <input type="number" class="search_prop_input_min" placeholder="Min Price">
            <input type="number" class="search_prop_input_max" placeholder="Max Price">
            <button type="button" class="search_prop_info"><i class="ti-search"></i></button>
        </div>
    </div>
</div>
<div class="content">
    <div class="container-fluid" id="property_list">
        <?php foreach ($properties as $key => $prop_data):?>
            <div class="col-lg-4 col-md-5">
                <div class="card card-user">
                    <div class="image">
                        <img src="<?=base_url()?><?=json_decode($prop_data['images'])[0]?>" alt="..."/>
                    </div>
                    <div class="content">
                        <div class="author">
                            <h4 class="title"><?=$prop_data['property_name']?><br/>
                                <a href="#"><small>View Property Page</small></a>
                            </h4>
                        </div>
                        <p class="description text-center">
                            <?=$prop_data['property_location']?>
                        </p>
                        <p class="text-center" style="margin-bottom: 0;"><b>₱<?=$prop_data['selling_price']?>.00</b></p>
                        <?php if($prop_data['sale_type'] == 1): ?>
                            <?php if($prop_data['availability'] == 1): ?>
                                <p class="text-center" style="margin-bottom: 0;"><b>Rent Until : <?=$prop_data['rent_deadline']?></b></p>
                            <?php else:?>
                                <p class="text-center" style="margin-bottom: 0;"><b>Not Available Until : <?=$prop_data['rent_deadline']?> </b></p>
                            <?php endif;?>
                        <?php endif;?>
                    </div>
                    <hr>
                    <div class="text-center management_btn">
                        <div class="col-sm-6">
                            <a href="<?=base_url()?>Admin/Manage/Property/<?=$prop_data['pid']?>">
                                <button class="btn btn-info">Manage</button>
                            </a>
                        </div>
                        <div class="col-sm-6">
                            <button class="btn btn-info btn-danger delete_property" data-id="<?=$prop_data['pid']?>">Delete</button>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach;?>
    </div>

    <?php if($pages > 1): ?>
    <div class="pagination_area">
        <div class="controls first_page">
            <i class="fa fa-angle-double-left"></i>
        </div>
        <div class="controls prev_page">
            <i class="fa fa-angle-left"></i>
        </div>
        <div class="current_page">
            <input type="number" value="1" max="<?=$pages?>">
            <span style="margin: auto 0 auto 5px;">of </span>
            <span class="total_page" style="margin: auto 5px;"><?=$pages?></span>
        </div>
        <div class="controls next_page">
            <i class="fa fa-angle-right"></i>
        </div>
        <div class="controls last_page">
            <i class="fa fa-angle-double-right"></i>
        </div>
    </div>
    <?php endif;?>

    <div id="success_modal" data-izimodal-title="Success" data-izimodal-subtitle="Account Successfully Deleted!">
    </div>
    <div id="notif_modal" data-izimodal-title="Notification" data-izimodal-subtitle="Please Fill up all the Fields."></div>
</div>