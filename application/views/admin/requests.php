<div class="content">
    <div class="container-fluid">
        <div class="row">
        	<div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Transaction List</h4>
                        <p class="category">Here is a list of all Accounts</p>
                    </div>
                    <div class="content table-responsive table-full-width">
                        <table class="table table-striped">
                            <thead>
                                <th>Transaction ID</th>
                            	<th>Client Details</th>
                            	<th>Property Details</th>
                            	<th>Status</th>
                            	<th>Action</th>
                            </thead>
                            <tbody id="requests_body">
                                <?php foreach ($list as $key => $val): ?>
                                <tr>
                                	<td><?=$val['id']?></td>
                                	<td>
                                        <b>Name: </b><?=$val['fname'].' '.$val['mname'].' '.$val['lname']?><br />
                                        <b>Email: </b><?=$val['email']?> <br />
                                        <b>Mobile: </b><?=$val['contact_number']?> <br />
                                    </td>
                                	<td>
                                        <b>Property Name: </b><?=$val['property_name']?> <br />
                                        <b>Location: </b><?=$val['property_location']?> <br />
                                        <b>Type: </b><?php 
                                            if($val['sale_type'] == 1){
                                                echo 'Rent';
                                            }else if($val['sale_type'] == 2){
                                                echo 'Transient';
                                            }else{
                                                echo 'For Sale';
                                            }
                                            ?> <br />   
                                        <b>Price: </b><?=$val['selling_price']?> <br />   
                                    </td>
                                	<td>
                                     <?php
                                        switch ($val['status']) {
                                            case 0:
                                                echo "Pending";
                                                break;
                                            case 1:
                                                echo "Messaged";
                                                break;
                                            case 2:
                                                echo "Paid";
                                                break;
                                            case 3:
                                                echo "Confirmed / Sold";
                                                break;
                                            default:
                                                echo "Pending";
                                                break;
                                        }
                                      ?>   
                                    </td>
                                	<td>
                                        <?php
                                            switch ($val['status']) {
                                                case 0:
                                                    echo '<button class="btn btn-success btn-icon btn-sm email_modal" data-id="'.$val['id'].'"><i class="ti-email"></i></button>';
                                                    break;
                                                case 1:
                                                    echo '<button class="btn btn-success btn-icon btn-sm payment_notif"><i class="ti-money"></i></button>';
                                                    break;
                                                case 2:
                                                    echo '<button class="btn btn-success btn-icon btn-sm confirm_payment_modal" data-id="'.$val['id'].'" data-prop="'.$val['property_id'].'"><i class="ti-thumb-up"></i></button>';
                                                    break;
                                                default:
                                                    echo '';
                                                    break;
                                            }
                                        ?>

                                        <button class="btn btn-danger btn-icon btn-sm delete_requests" data-id="<?=$val['id']?>"><i class="ti-trash"></i></button>
                                        <button class="btn btn-danger btn-icon btn-sm mark_as_rented" data-id="<?=$val['property_id']?>" value="<?=$val['availability']?>">
                                            <?php if($val['availability'] == 1):?>
                                                Mark as unavailable
                                                <?php else:?>
                                                    Mark as available
                                                <?php endif;?>
                                            </button>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>

                        <?php if($pages > 1): ?>
                        <div class="pagination_area">
                            <div class="controls first_page">
                                <i class="fa fa-angle-double-left"></i>
                            </div>
                            <div class="controls prev_page">
                                <i class="fa fa-angle-left"></i>
                            </div>
                            <div class="current_page">
                                <input type="number" value="1" max="<?=$pages?>">
                                <span style="margin: auto 0 auto 5px;">of </span>
                                <span class="total_page" style="margin: auto 5px;"><?=$pages?></span>
                            </div>
                            <div class="controls next_page">
                                <i class="fa fa-angle-right"></i>
                            </div>
                            <div class="controls last_page">
                                <i class="fa fa-angle-double-right"></i>
                            </div>
                        </div>
                        <?php endif;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="status_modal1" style="text-align: center;">
    <div style="padding: 20px 20px 50px;">
        <h2><b>Notify the Client!</b></h2>
        <p>Call, send a Message or Email the Client regarding on the details of the Property Purchase and Brief them on other Important matter.</p>
        <!-- <button class="btn btn-info">Email Client</button> -->
        <button class="btn btn-fill btn-success change_email_status">Already Messaged</button>
    </div>
</div>

<div id="status_modal2" style="text-align: center;">
    <div style="padding: 20px 20px 50px;">
        <input type="hidden" class="data_details">
        <h2><b>Payment Time!</b></h2>
        <p>Contact the Buyer and ask them to pay the amount in the Banks listed in their Interface then ask them to upload the receipt to complete the transaction.</p>
    </div>
</div>

<div id="status_modal3" style="text-align: center;">
    <div style="padding: 20px 20px 50px;">
        <h2><b>Payment Confirmation.</b></h2>
        <p>Please Check your Account if you have recieved the paid ammount. Once confirmed, click the button below to process the Transaction.</p>
        <button class="btn btn-fill btn-success confirm_payment">Confirm Payment</button>
    </div>
</div>

<div id="success_modal" data-izimodal-title="Success" data-izimodal-subtitle="Account Successfully Deleted!">
</div>