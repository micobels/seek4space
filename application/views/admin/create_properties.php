<style type="text/css">
    .property_image_preview_section,.legal_image_preview_section{
        list-style: none;
        padding: 0;
    }
    .property_image_preview_section li,.legal_image_preview_section li{
        display: inline-block;
        width: 15%;
        border: solid 3px #e2e2e2;
        border-radius: 5px;
        margin: 2px;
        height: 10vh;
    }
    .property_image_preview_section li img,.legal_image_preview_section li img{
        width: 100%;
        object-fit: contain;
        height: 9vh;
        padding: 2px;
    }
</style>
<div class="content">
    <div class="container-fluid">
        <div class="row">
        	<div class="col-sm-12">
                <div class="card property_create_form">
                    <div class="header">
                        <h4 class="title">Add New Property</h4>
                    </div>
                    <div class="content">
                        <form method="POST" action="#" enctype="multipart/form-data" id="add_property_serialize">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Property Name</label>
                                        <input type="text" class="form-control border-input validate prop_name" placeholder="Property Name">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Property Location</label>
                                        <input type="text" class="form-control border-input validate prop_location" placeholder="Property Location">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Sale Type</label>
                                        <select class="form-control prop_sale_type validate">
                                            <option value='0'>Choose One</option>
                                            <option value='1'>Rent</option>
                                            <option value='2'>Transient</option>
                                            <option value='3'>For Sale</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Original Price</label>
                                        <input type="number" class="form-control border-input validate prop_orig_price">
                                    </div>
                                </div> -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Selling Price | Rental Fee</label>
                                        <input type="number" class="form-control border-input validate prop_sell_price" >
                                    </div>
                                </div>
                                <div class="col-md-4 rental_info" style="display: none;">
                                    <div class="form-group">
                                        <label>Rent Until</label><br>
                                        <input type="date" class="rent_date validate form-control border-input">
                                    </div>
                                </div>
                            </div>
                        
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Property Images</label><br>
                                        <ul class="property_image_preview_section"></ul>
                                        <label class="btn btn-simple btn-fill btn-wd" for="property_images">
                                            Upload Image
                                            <input type="file" name="property_images[]" class="validate" multiple id="property_images" style="display: none;">
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Legal Documents</label><br>
                                        <ul class="legal_image_preview_section"></ul>
                                        <label class="btn btn-simple btn-fill btn-wd" for="legal_document">Upload Image
                                        <input type="file" name="legal_document[]" class="validate" multiple id="legal_document" style="display: none;">
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Embed Map URL</label><br>
                                        <input type="text" class="form-control border-input validate embed_map">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Property Description</label><br>
                                        <select class="desc_drop form-control">
                                            <option value="Bedroom">Bedroom</option>
                                            <option value="House color">House color</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <textarea rows="5" class="form-control border-input desc_input"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="text-right">
                                <button type="submit" class="btn btn-info btn-fill btn-wd add_property_btn">Add Property</button>
                            </div>
                            <div class="clearfix"></div>

                            <div id="success_modal" data-izimodal-title="Success" data-izimodal-subtitle="Property Successfully Created."></div>
                            <div id="notif_modal" data-izimodal-title="Notification" data-izimodal-subtitle="Please Fill up all the Fields."></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>