<?php if($table == 'accounts'): ?>
    <?php if(!empty($list)):?>
        <?php foreach ($list as $key => $val): ?>
        <tr>
            <td><?=$val['user_id']?></td>
            <td><?=$val['fname'].' '.$val['mname'].' '.$val['lname']?></td>
            <td><?=$val['email']?></td>
            <td><?=$val['contact_number']?></td>
            <td><?php
            switch ($val['user_type']) {
                case 0:
                    echo 'Super Admin';
                    break; 
                case 1:
                    echo 'Buyer';
                    break; 
                case 2:
                    echo 'Seller';
                    break; 
                default:
                    echo 'Super Admin';
                    break;
            }
            ?></td>
            <td>
                <button class="btn btn-success btn-icon btn-sm account_details" data-id="<?=$val['user_id']?>"><i class="ti-eye"></i></button>
                <button class="btn btn-danger btn-icon btn-sm delete_account" data-id="<?=$val['user_id']?>"><i class="ti-trash"></i></button>
            </td>
        </tr>
        <?php endforeach; ?>
    <?php else: ?>
    <tr>
        <td colspan="6" style="text-align: center;">
            No Products to Load
        </td>
    </tr>
    <?php endif; ?>
<?php endif; ?>

<?php if($table == 'properties'): ?>
    <?php if(!empty($properties)):?>
        <?php foreach ($properties as $key => $prop_data):?>
            <div class="col-lg-4 col-md-5">
                <div class="card card-user">
                    <div class="image">
                        <img src="<?=base_url()?><?=json_decode($prop_data['images'])[0]?>" alt="..."/>
                    </div>
                    <div class="content">
                        <div class="author">
                            <h4 class="title"><?=$prop_data['property_name']?><br/>
                                <a href="#"><small>View Property Page</small></a>
                            </h4>
                        </div>
                        <p class="description text-center">
                            <?=$prop_data['property_location']?>
                        </p>
                        <p class="text-center" style="margin-bottom: 0;"><b>₱<?=$prop_data['selling_price']?>.00</b></p>
                        <?php if($prop_data['sale_type'] == 1): ?>
                            <?php if($prop_data['availability'] == 1): ?>
                                <p class="text-center" style="margin-bottom: 0;"><b>Rent Until : <?=$prop_data['rent_deadline']?></b></p>
                            <?php else:?>
                                <p class="text-center" style="margin-bottom: 0;"><b>Not Available Until : <?=$prop_data['rent_deadline']?> </b></p>
                            <?php endif;?>
                        <?php endif;?>
                    </div>
                    <hr>
                    <div class="text-center management_btn">
                        <div class="col-sm-6">
                            <a href="<?=base_url()?>Admin/Manage/Property/<?=$prop_data['pid']?>">
                                <button class="btn btn-info">Manage</button>
                            </a>
                        </div>
                        <div class="col-sm-6">
                            <button class="btn btn-info btn-danger delete_property" data-id="<?=$prop_data['pid']?>">Delete</button>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach;?>
    <?php else: ?>

    <?php endif; ?>
<?php endif; ?>

<?php if($table == 'requests'): ?>
    <?php if(!empty($list)):?>
        <?php foreach ($list as $key => $val): ?>
        <tr>
            <td><?=$val['id']?></td>
            <td>
                <b>Name: </b><?=$val['fname'].' '.$val['mname'].' '.$val['lname']?><br />
                <b>Email: </b><?=$val['email']?> <br />
                <b>Mobile: </b><?=$val['contact_number']?> <br />
            </td>
            <td>
                <b>Property Name: </b><?=$val['property_name']?> <br />
                <b>Location: </b><?=$val['property_location']?> <br />
                <b>Type: </b><?php 
                    if($val['sale_type'] == 1){
                        echo 'Rent';
                    }else if($val['sale_type'] == 2){
                        echo 'Transient';
                    }else{
                        echo 'House and Lot';
                    }
                    ?> <br />   
                <b>Price: </b><?=$val['selling_price']?> <br />   
            </td>
            <td>
             <?php
                switch ($val['status']) {
                    case 0:
                        echo "Pending";
                        break;
                    case 1:
                        echo "Messaged";
                        break;
                    case 2:
                        echo "Paid";
                        break;
                    case 3:
                        echo "Confirmed / Sold";
                        break;
                    default:
                        echo "Pending";
                        break;
                }
              ?>   
            </td>
            <td>
                <?php
                    switch ($val['status']) {
                        case 0:
                            echo '<button class="btn btn-success btn-icon btn-sm email_modal" data-id="'.$val['id'].'"><i class="ti-email"></i></button>';
                            break;
                        case 1:
                            echo '<button class="btn btn-success btn-icon btn-sm payment_notif"><i class="ti-money"></i></button>';
                            break;
                        case 2:
                            echo '<button class="btn btn-success btn-icon btn-sm confirm_payment_modal" data-id="'.$val['id'].'" data-prop="'.$val['property_id'].'"><i class="ti-thumb-up"></i></button>';
                            break;
                        default:
                            echo '';
                            break;
                    }
                ?>
                <button class="btn btn-danger btn-icon btn-sm delete_requests" data-id="<?=$val['id']?>"><i class="ti-trash"></i></button>
                <button class="btn btn-danger btn-icon btn-sm mark_as_rented" data-id="<?=$val['property_id']?>" value="<?=$val['availability']?>">
                    <?php if($val['availability'] == 1):?>
                        Mark as unavailable
                    <?php else:?>
                        Mark as available
                    <?php endif;?>
                </button>
            </td>
        </tr>
        <?php endforeach; ?>
    <?php else: ?>
    <tr>
        <td colspan="5" style="text-align: center;">
            No Requests to Load
        </td>
    </tr>
    <?php endif; ?>
<?php endif; ?>