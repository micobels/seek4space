<style type="text/css">
	.dashboard_holder{
		position: relative;
		height: 77vh;
	}
	.dashboard_holder .img_holder{
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translate(-50%,-50%);
		width: 80%;
		text-align: center;
	}
	.dashboard_holder b{
		color: #105a9c;
	}
</style>

<div class="dashboard_holder">
	<div class="img_holder">
		<img src="<?=base_url()?>assets/img/seek4space.png">
		<h1>WELCOME TO <b>SEEK4SPACE</b></h1>
	</div>
</div>