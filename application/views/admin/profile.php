<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 col-md-5">
                <div class="card card-user">
                    <div class="image">
                        <img src="<?=base_url()?>assets/img/background.jpg" alt="..."/>
                    </div>
                    <div class="content">
                        <div class="author">
                          <img class="avatar border-white" src="<?=base_url()?>assets/img/avatar.jpg" alt="..."/>
                          <h4 class="title"><?=$user['fname'].' '.$user['mname'].' '.$user['lname']?><br />
                             <a href="#"><small>@<?=$user['fname'].'_'.$user['lname']?></small></a>
                          </h4>
                        </div>
                        <p class="description text-center">
                            "<?=$user['about']?>"
                        </p>
                    </div>
                    <hr>
                    <div class="text-center">
                        <div class="row">
                            <div class="col-md-3 col-md-offset-1">
                                <h5><a href="<?=$user['fb_link']?>"><i class="fa fa-facebook"></i></a><br /><small>Facebook</small></h5>
                            </div>
                            <div class="col-md-4">
                                <h5><a href="<?=$user['insta_link']?>"><i class="fa fa-instagram"></i></a><br /><small>Instagram</small></h5>
                            </div>
                            <div class="col-md-3">
                                <h5><a href="#"><i class="fa fa-envelope-o"></i></a><br /><small>Email</small></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-7">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Edit Profile</h4>
                    </div>
                    <div class="content">
                        <form>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Seek4Space Account ID</label>
                                        <input type="text" class="form-control border-input user_id" disabled placeholder="Company" value="<?=$user['user_id']?>">
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email address</label>
                                        <input type="email" class="form-control border-input validate email_address" placeholder="Email" value="<?=$user['email']?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>First Name</label>
                                        <input type="text" class="form-control border-input validate fname" placeholder="First Name" value="<?=$user['fname']?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Middle Name</label>
                                        <input type="text" class="form-control border-input validate mname" placeholder="Middle Name" value="<?=$user['mname']?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <input type="text" class="form-control border-input validate lname" placeholder="Last Name" value="<?=$user['lname']?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <!-- <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Age</label>
                                        <input type="number" class="form-control border-input validate age" placeholder="Age" value="<?=$user['age']?>">
                                    </div>
                                </div> -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Birth Date</label>
                                        <input type="date" class="form-control border-input validate birth_date" value="<?=$user['birth_date']?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Place of Birth</label>
                                        <input type="text" class="form-control border-input validate birth_place" placeholder="Place of Birth" value="<?=$user['birth_place']?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Address</label>
                                        <input type="text" class="form-control border-input validate address" placeholder="Home Address" value="<?=$user['address']?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Civil Status</label>
                                        <select class="form-control validate civil_status" value="<?=$user['civil_status']?>">
                                            <option value="1" <?=$user['civil_status'] == 1 ? 'selected' : ''?>>Single</option>
                                            <option value="2" <?=$user['civil_status'] == 2 ? 'selected' : ''?>>Married</option>
                                            <option value="3" <?=$user['civil_status'] == 3 ? 'selected' : ''?>>Widowed</option>
                                            <option value="4" <?=$user['civil_status'] == 4 ? 'selected' : ''?>>divorced</option>
                                            <option value="5" <?=$user['civil_status'] == 5 ? 'selected' : ''?>>separated</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="form-group">
                                        <label for="exampleFormControlSelect1">Gender</label>
                                        <select class="form-control validate gender" value="<?=$user['gender']?>">
                                            <option value="1" <?=$user['gender'] == 1 ? 'selected' : ''?>>Male</option>
                                            <option value="2" <?=$user['gender'] == 2 ? 'selected' : ''?>>Female</option>
                                            <option value="3" <?=$user['gender'] == 3 ? 'selected' : ''?>>Others</option>
                                        </select>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Nationality</label>
                                        <input type="text" class="form-control border-input validate nationality" placeholder="Nationality" value="<?=$user['nationality']?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <!-- <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Citizenship</label>
                                        <input type="text" class="form-control border-input validate citizenship" placeholder="Citizenship" value="<?=$user['citizenship']?>">
                                    </div>
                                </div> -->
                                <!-- <div class="col-md-4">
                                    <div class="form-group">
                                        <label>No. of Dependents</label>
                                        <input type="number" class="form-control border-input validate dependents" value="<?=$user['no_dependents']?>">
                                    </div>
                                </div> -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Postal Code</label>
                                        <input type="number" class="form-control border-input validate postal_code" placeholder="ZIP Code" value="<?=$user['postal_code']?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Contact Number</label>
                                        <input type="number" class="form-control border-input validate mobile" placeholder="Mobile Number" value="<?=$user['contact_number']?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Bank Account Number</label>
                                        <input type="number" class="form-control border-input validate bank_account" value="<?=$user['bank_account']?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>About Me</label>
                                        <textarea rows="5" class="form-control border-input validate about" placeholder="Here can be your description">
                                            <?=$user['about']?>
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-info btn-fill btn-wd update_profile">Update Profile</button>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="success_modal" data-izimodal-title="Success" data-izimodal-subtitle="Profile Updated Successfully."></div>
<div id="notif_modal" data-izimodal-title="Notification" data-izimodal-subtitle="Please Fill up all the Fields."></div>