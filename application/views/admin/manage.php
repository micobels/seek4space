<style type="text/css">
    .house_img img{
        width: 100%;
        border: solid 3px #d3d3d3;
        cursor: pointer;
        height: 60px;
        object-fit: cover;
        padding: 3px;
    }
    .house_img li{
        width: 24%;
        padding: 5px;
        height: 70px;
    }
    #property_document_modal .preview_document_cont img{
        margin: 20px auto;
        display: block;
        max-width: 500px;
        object-fit: contain;
        border: solid 5px #e9e9e9;
    }
    #property_document_modal li{
        width: 16.667%;
    }
    #property_document_modal li img{
        width: 100%;
        height: 70px;
        border: solid 3px #c8dcff;
        cursor: pointer;
    }
</style>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 col-md-5">
                <div class="card card-user">
                    <div class="image">
                        <img src="<?=base_url().json_decode($list['images'])[0]?>" alt="..."/>
                    </div>
                    <div class="content" style="text-align: center;">
                        <ul class="list-inline house_img">
                            <?php
                                $images = json_decode($list['images']);
                                foreach ($images as $key => $value):
                            ?>
                            <li><img src="<?=base_url().$value?>"></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <hr>
                    <div class="text-center">
                        <div class="row">
                            <!-- <div class="col-md-3 col-md-offset-1">
                                <a href="#"><h5><i class="fa fa-barcode"></i><br /><small>Mark</small></h5></a>
                            </div> -->
                            <?php if($user['user_type'] != 0): ?>
                            <div class="col-md-12">
                            <?php else: ?>
                            <div class="col-md-6">
                            <?php endif; ?>
                                <a style="cursor: pointer;" class="open_documents"><h5><i class="fa fa-file"></i><br /><small>documents</small></h5></a>
                            </div>
                            <?php if($user['user_type'] == 0): ?>
                            <div class="col-md-6">
                                <?php if($list['status'] == 0): ?>
                                <a style="cursor: pointer;" class="activate_prop"><h5><i class="fa fa-eye"></i><br /><small>Activate</small></h5></a>
                                <?php else: ?>
                                <a style="cursor: pointer;" class="deactivate_prop"><h5><i class="fa fa-eye-slash"></i><br /><small>Deactivate</small></h5></a>
                                <?php endif; ?>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-7">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Property Management</h4>
                    </div>
                    <div class="content">
                        <form>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Seek4Space Property ID</label>
                                        <input type="text" class="form-control border-input property_id" disabled placeholder="Seek4Space Property ID." value="<?=$list['pid']?>">
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Property Status</label>
                                        <select class="form-control validate up_status" value="<?=$list['sale_type']?>">
                                            <!-- <option value='0' <?=$list['sale_type'] == 0 ? 'selected' :''?>>Choose One</option> -->
                                            <option value='1' <?=$list['sale_type'] == 1 ? 'selected' :''?>>Rent</option>
                                            <option value='2' <?=$list['sale_type'] == 2 ? 'selected' :''?>>Transient</option>
                                            <option value='3' <?=$list['sale_type'] == 3 ? 'selected' :''?>>For Sale</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Property Name</label>
                                        <input type="text" class="form-control border-input validate up_name" placeholder="Property Name" value="<?=$list['property_name']?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Property Address</label>
                                        <input type="text" class="form-control border-input validate up_address" placeholder="Property Address" value="<?=$list['property_location']?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <!-- <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Original Price</label>
                                        <input type="number" class="form-control border-input validate up_orig" placeholder="Original Price" value="<?=$list['original_price']?>">
                                    </div>
                                </div> -->
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Selling Price | Rental Fee</label>
                                        <input type="number" class="form-control border-input validate up_selling" placeholder="Selling Price" value="<?=$list['selling_price']?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Property Description</label>
                                        <textarea class="form-control border-input validate description" rows="5"><?=$list['description']?></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="text-center">
                                <button type="submit" class="btn btn-info btn-fill btn-wd update_property">Update Profile</button>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="success_modal" data-izimodal-title="Success" data-izimodal-subtitle="Property Update Successful!">
</div>
<div id="notif_modal" data-izimodal-title="Notification" data-izimodal-subtitle="Please Fill up all the Fields."></div>
<div id="property_document_modal" style="max-height: 600px;">
</div>