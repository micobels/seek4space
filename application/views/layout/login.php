
<section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
    	<div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
		<div class="container">
			<div class="banner_content" style="position: absolute;bottom: 30%;">
				<div class="page_link">
					<a href="<?=base_url()?>">Home</a>
					<a href="<?=base_url()?>Login">Login</a>
				</div>
				<h2>Login</h2>
			</div>
		</div>
    </div>
   	<div class="container login_form">
		<div class="advanced_search">
			<h3>Lorem Ipsum Dolor Simet</h3>
			<div class="search_select">
				<div style="display: flex;">
					<i class="fa fa-user"></i>
					<input type="email" placeholder="Email Address" class="main_input login_email">
				</div>
				<div style="display: flex;">
					<i class="fa fa-lock" style="padding: 10px 11px;"></i>
                	<input type="password" placeholder="Password" class="main_input login_password">
                </div>
			</div>
			<button type="button" value="submit" class="btn submit_btn login_btn">Login</button>
			<p style="margin-top: 40px;">* No Account Yet, <B><a href="<?=base_url()?>Register">Register Here.</a></B></p>
		</div>
    </div>
</section>

<div id="notif_modal" data-izimodal-title="Notification" data-izimodal-subtitle="Email or Password is Incorrect."></div>

<style type="text/css">
	.advanced_search{
		z-index: 2;
		margin-top: -200px;
		margin-bottom: 50px;
	}
	.search_select i{
	    padding: 10px;
	    border: solid 1px #d3d3d3;
	    margin: auto auto 10px;
	    border-right: none;
	}
	.main_input{
		border-left: none;
	}
</style>