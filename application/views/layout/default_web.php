<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" sizes="96x96" href="<?=base_url()?>assets/img/ico.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Seek4Space | <?=isset($page_title) ? $page_title : ''?></title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link href="<?=base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?=base_url();?>assets/vendors/linericon/style.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/css/web/style.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/css/web/responsive.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/css/slick.css"/>
    <link href="<?=base_url();?>assets/css/iziModal.min.css" rel="stylesheet">
</head>
<body>
	<?php $header=(isset($header)) ? $header:''; echo $header;?>
	<?php $build_Data=(isset($build_Data)) ? $build_Data:''; echo $build_Data; ?>
	<?php $footer=(isset($footer)) ? $footer:''; echo $footer; ?>
</body>
<script src="<?=base_url();?>assets/js/jquery.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/js/slick.js"></script>
<?php if(isset($js_array)):?>
	<?php foreach ($js_array as $key => $path):?>
		<script src="<?=$path?>" ></script>
	<?php endforeach;?>
<?php endif;?>
<script type="text/javascript">
	base_url = '<?=base_url()?>';
	$('.center').slick({
	  	centerMode: true,
	  	centerPadding: '60px',
	  	slidesToShow: 3,
	  	arrows: true,
    	prevArrow:"<button type='button' class='slick-prev pull-left'><i class='fa fa-angle-left' aria-hidden='true'></i></button>",
    	nextArrow:"<button type='button' class='slick-next pull-right'><i class='fa fa-angle-right' aria-hidden='true'></i></button>",
	  	responsive: [
	    	{
	      		breakpoint: 768,
		      	settings: {
		        	arrows: false,
		        	centerMode: true,
		        	centerPadding: '40px',
		        	slidesToShow: 3
		      		}
	    	},
	    	{
		      	breakpoint: 480,
	      		settings: {
		        	arrows: false,
		        	centerMode: true,
		        	centerPadding: '40px',
		        	slidesToShow: 1
		      	}
	    	}
	  	]
	});

	$('.logout_session').click(function(){
		$.ajax({
			type  	: 'POST',
			url 	: base_url+"Login/logout",
			cache	: false,
			dataType : 'json',
			success : function(r){
				window.location.href = base_url;
			},
			error 	: function(e){
				console.log('error');
			}
		})
	})
</script>
</html>