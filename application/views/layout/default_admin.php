<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" sizes="96x96" href="<?=base_url()?>assets/img/ico.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Seek4Space | <?=isset($page_title) ? $page_title : ''?></title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link href="<?=base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?=base_url();?>assets/css/animate.min.css" rel="stylesheet"/>
    <link href="<?=base_url();?>assets/css/paper-dashboard.css" rel="stylesheet"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="<?=base_url();?>assets/css/themify-icons.css" rel="stylesheet">
    <link href="<?=base_url();?>assets/css/iziModal.min.css" rel="stylesheet">
</head>
<body>

<div class="wrapper">
	<?php $header=(isset($header)) ? $header:''; echo $header;?>
	<?php $build_Data=(isset($build_Data)) ? $build_Data:''; echo $build_Data; ?>
	<?php $footer=(isset($footer)) ? $footer:''; echo $footer; ?>
</div>

</body>
<script src="<?=base_url();?>assets/js/jquery.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/js/bootstrap-checkbox-radio.js"></script>
<script src="<?=base_url();?>assets/js/bootstrap-notify.js"></script>
<script src="<?=base_url();?>assets/js/paper-dashboard.js"></script>
<script type="text/javascript">
	base_url = '<?=base_url()?>';
	$('.logout_session').click(function(){
		$.ajax({
			type  	: 'POST',
			url 	: base_url+"Login/logout",
			cache	: false,
			dataType : 'json',
			success : function(r){
				window.location.href = base_url;
			},
			error 	: function(e){
				console.log('error');
			}
		})
	})
</script>
<?php if(isset($js_array)):?>
	<?php foreach ($js_array as $key => $path):?>
		<script src="<?=$path?>" ></script>
	<?php endforeach;?>
<?php endif;?>
</html>