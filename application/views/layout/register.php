
<section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
    	<div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
		<div class="container">
			<div class="banner_content" style="position: absolute;bottom: 30%;">
				<div class="page_link">
					<a href="<?=base_url()?>">Home</a>
					<a href="<?=base_url()?>Register">Register</a>
				</div>
				<h2>Register</h2>
			</div>
		</div>
    </div>
   	<div class="container">
		<div class="advanced_search">
			<div class="register_form">
				<form id="registration_form">
					<div class="initial_form">
						<div class="col-sm-6">
							<label>User Type</label>
							<select class="form-control reg_utype validate">
                                <option value="1">Buyer</option>
                                <option value="2">Seller</option>
                            </select>
						</div>
						<div class="col-sm-6">
							<label>Email Address</label>
							<input type="email" class="main_input reg_email validate">
						</div>
					</div>
					<div class="hidden_form">
						<div class="row_divide">
							<div class="col-sm-4">
								<label>First Name</label>
								<input type="text" class="main_input reg_fname validate">
							</div>
							<div class="col-sm-4">
								<label>Middle Name</label>
								<input type="text" class="main_input reg_mname validate">
							</div>
							<div class="col-sm-4">
								<label>Last Name</label>
								<input type="text" class="main_input reg_lname validate">
							</div>
						</div>
						<div class="row_divide">
							<!-- <div class="col-sm-3">
								<label>Age</label>
								<input type="number" class="main_input reg_age validate">
							</div> -->
							<div class="col-sm-6">
								<label>Birth Date</label>
								<input type="Date" class="main_input reg_birth_date validate">
							</div>
							<div class="col-sm-6">
								<label>Birth Place</label>
								<input type="text" class="main_input reg_birth_place validate">
							</div>
						</div>
						<div class="row_divide">
							<!-- <div class="col-sm-12">
								<label>Address</label>
								<input type="text" class="main_input reg_address validate">
							</div> -->
							<div class="col-sm-2 text-left">
								<label>House No.</label>
								<input type="number" class="main_input house_num validate">
							</div>
							<div class="col-sm-4">
								<label>Street</label>
								<input type="text" class="main_input house_street validate">
							</div>
							<div class="col-sm-3">
								<label>City</label>
								<input type="text" class="main_input house_city validate">
							</div>
							<div class="col-sm-3">
								<label>Province</label>
								<input type="text" class="main_input house_prov validate">
							</div>
						</div>
						<div class="row_divide">
							<div class="col-sm-4">
								<label>Civil Status</label>
								<select class="form-control reg_civil_stat validate">
	                                <option value="1">Single</option>
	                                <option value="2">Married</option>
	                                <option value="3">Widowed</option>
	                                <option value="4">divorced</option>
	                                <option value="5">separated</option>
	                            </select>
							</div>
							<div class="col-sm-4">
								<label>Gender</label>
								<select class="form-control reg_gender validate">
	                                <option value="1">Male</option>
	                                <option value="2">Female</option>
	                                <option value="3">Others</option>
	                            </select>
							</div>
							<div class="col-sm-4">
								<label>Nationality</label>
								<input type="text" class="main_input reg_nationality validate">
							</div>
						</div>
						<div class="row_divide">
							<!-- <div class="col-sm-6">
								<label>Citizenship</label>
								<input type="text" class="main_input reg_citizenship validate">
							</div> -->
							<!-- <div class="col-sm-4">
								<label>Number of Dependents</label>
								<input type="number" class="main_input reg_dependents validate">
							</div> -->
							<div class="col-sm-6">
								<label>Postal Code</label>
								<input type="number" class="main_input reg_postal validate">
							</div>
							<div class="col-sm-6">
								<label>Contact Number</label>
								<input type="number" class="main_input reg_contact_no validate">
							</div>
						</div>
						<div class="row_divide">
							<div class="col-sm-12">
								<label>Bank Account</label>
								<input type="text" class="main_input reg_bank_account validate">
							</div>
						</div>
						<div class="row_divide">
							<div class="col-sm-12">
								<label>About Me</label>
								<textarea class="main_input reg_about validate" rows="5"></textarea>
							</div>
						</div>
						<div class="row_divide">
                            <div class="col-sm-6">
                                <label>Facebook Link</label>
                                <input type="text" class="main_input reg_facebook validate">
                            </div>
                            <div class="col-sm-6">
                                <label>Instagram Link</label>
                                <input type="text" class="main_input reg_instagram validate">
                            </div>
                        </div>
						<div class="row_divide">
							<div class="col-sm-6">
								<label>Password</label>
								<input type="password" class="main_input reg_password validate">
							</div>
							<div class="col-sm-6">
								<label>Confirm Password</label>
								<input type="password" class="main_input reg_confirm_password validate">
							</div>
						</div>
					</div>
				</form>
			</div>
			<button type="text" value="submit" class="btn submit_btn proceed_register_btn">Register</button>
		</div>
    </div>
    <div id="notif_modal" data-izimodal-title="Notification" data-izimodal-subtitle="Email or Password is Incorrect."></div>

</section>

<style type="text/css">
	.advanced_search{
		z-index: 2;
		margin-top: -200px;
		margin-bottom: 50px;
		max-width: 800px;
		border: solid 1px #eaeaea;
	}
	#registration_form .col-sm-4,
	#registration_form .col-sm-3,
	#registration_form .col-sm-5,
	#registration_form .col-sm-6,
	#registration_form .col-sm-12{
		text-align: left;
	}
	#registration_form label{
	    color: #888888;
    	font-size: 12px;
	}
	#registration_form .row_divide,
	#registration_form .hidden_form,
	#registration_form .initial_form{
		margin-bottom: 5px;
		display: inline-block;
		width: 100%;
	}
	#registration_form .main_input{
		border-radius: 5px;
		color: #555;
	}
</style>