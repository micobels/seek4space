<header class="header_area">
    <div class="main_menu">
    	<nav class="navbar navbar-expand-lg navbar-light">
			<div class="container box_1620">
				<a class="navbar-brand logo_h" href="<?=base_url()?>"><img src="<?=base_url();?>assets/img/seek4space.png" alt=""></a>
				<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
					<ul class="nav navbar-nav menu_nav ml-auto pull-right">
						<li class="nav-item"><a class="nav-link" href="<?=base_url()?>">Home</a></li> 
						<li class="nav-item"><a class="nav-link" href="<?=base_url()?>Client/About_us">About</a></li>
						<li class="nav-item"><a class="nav-link" href="<?=base_url()?>Client/Properties">Properties</a></li>
						<li class="nav-item"><a class="nav-link" href="<?=base_url()?>Client/Contact_us">Contact</a></li>
						<li class="nav-item" style="margin: auto 20px;"><a class="nav-link" href="<?=base_url()?>Register" style="color: orange;"><i class="lnr lnr-tag"></i> Sell a Property</a></li>
						<?php if(!isset($user)): ?>
						<li class="nav-item" style="margin: auto 20px;"><a class="nav-link" href="<?=base_url()?>Client/Activate" style="color: #105a9c;"><i class="lnr lnr-diamond"></i> Activate Premium</a></li>
						<li class="nav-item"><a class="nav-link" href="<?=base_url()?>Login">Login</a></li>
						<?php else: ?>
							<?php if($user['premium'] == 0) : ?>
						<li class="nav-item" style="margin: auto 20px;"><a class="nav-link" href="<?=base_url()?>Client/Activate" style="color: #105a9c;"><i class="lnr lnr-diamond"></i> Activate Premium</a></li>
					<?php endif; ?>
						<li class="nav-item submenu dropdown">
							<a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Welcome <b><?=$user['fname']?></b></a>
							<ul class="dropdown-menu">
								<?php if($user['user_type'] != 1): ?>
								<li class="nav-item"><a class="nav-link" href="<?=base_url()?>Admin/Dashboard">Dashboard</a></li>
								<?php endif;?>
								<!-- <li class="nav-item"><a class="nav-link" href="<?=base_url()?>Client/Activate">Activate Premium</a></li> -->
								<li class="nav-item"><a class="nav-link logout_session" href="#">Logout</a></li>
							</ul>
						</li> 
						<?php endif; ?>
					</ul>
				</div> 
			</div>
    	</nav>
    </div>
</header>