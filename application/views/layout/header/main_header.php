<div class="sidebar" data-background-color="white" data-active-color="danger">
	<div class="sidebar-wrapper">
        <div class="logo">
            <a href="<?=base_url()?>" class="simple-text">
                <img src="<?=base_url()?>assets/img/seek4space.png">
            </a>
        </div>
        <ul class="nav">
            <li class="active dashboard">
                <a href="<?=base_url()?>Admin/Dashboard">
                    <i class="ti-panel"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <?php if(isset($user) && $user['user_type'] == 0): ?>
            <li class="accounts">
                <a href="<?=base_url()?>Admin/Accounts">
                    <i class="ti-user"></i>
                    <p>Accounts</p>
                </a>
            </li>
            <?php endif; ?>
            <li class="properties">
                <a href="<?=base_url()?>Admin/Properties">
                    <i class="ti-home"></i>
                    <p>Properties</p>
                </a>
            </li>
            <li class="requests">
                <a href="<?=base_url()?>Admin/Requests">
                    <i class="ti-comments"></i>
                    <p>Requests</p>
                </a>
            </li>
        </ul>
	</div>
</div>

<div class="main-panel">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar bar1"></span>
                    <span class="icon-bar bar2"></span>
                    <span class="icon-bar bar3"></span>
                </button>
                <!-- <a class="navbar-brand" href="#"><?=$page_title?></a> -->
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="ti-settings"></i>
							<p>Settings</p>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="<?=base_url()?>Admin/Profile">Profile</a></li>
                            <li><a href="#" class="logout_session">Logout</a></li>
                        </ul>
                    </li>
                </ul>

            </div>
        </div>
    </nav>