<!--================Home Banner Area =================-->
<section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
    	<div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
		<div class="container">
			<div class="banner_content" style="position: absolute;bottom: 30%;">
				<div class="page_link">
					<a href="<?=base_url()?>">Home</a>
					<a href="<?=base_url()?>Client/Properties">Properties</a>
				</div>
				<h2>Properties</h2>
			</div>
		</div>
    </div>
</section>
<!--================End Home Banner Area =================-->

<div class="container">
	<div class="property_search client_property_search">
		<h3>Search Properties for</h3>
		<div class="search_select">
			<div class="col-sm-12">
				<input type="text" name="Name" placeholder="Property Name | Property Location | Property Price" class="prop_name main_input main_search" style="text-align: center;">
			</div>
			<!-- <div class="col-sm-6">
	            <input type="text" name="Location" placeholder="Property Location" class="prop_location main_input">
	        </div>
	        <div class="col-sm-4">
	            <select class="s_select prop_type">
	                <option value="0">Choose Sale Type</option>
	                <option value="1">Rent</option>
	                <option value="2">Transient</option>
	                <option value="3">House and Lot</option>
	            </select>
	        </div>
	        <div class="col-sm-4">
	            <input type="number" name="min_price" placeholder="Min. Price" class="main_input prop_min_price">
	        </div>
	        <div class="col-sm-4">
	            <input type="number" name="max_price" placeholder="Max. Price" class="main_input prop_max_price">
	        </div> -->
		</div>
		<button type="submit" value="submit" class="btn submit_btn search_client_prop">Search Property</button>
	</div>
</div>

<section class="properties_area">
	<div class="container">
		<div class="main_title">
			<!-- <h2>Our Newest Properties</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</p> -->
		</div>
		<div class="row properties_inner">
			<?php foreach ($properties as $key => $value): ?>
				<div class="col-sm-4">
					<div class="properties_item">
						<div class="pp_img">
							<img class="img-fluid" src="<?=base_url().json_decode($value['images'])[0]?>" style="width: 100%;border-radius: 5px;height: 250px;">
						</div>
						<div class="pp_content">
							<a href="#"><h4><?=$value['property_name']?></h4></a>
							<div class="location">
								<p><b>Location:</b> <?=$value['rent_deadline']?></p>
							</div>
							<div class="deadline">
								<?php if($value['sale_type'] == 1): ?>
									<?php if($value['availability'] == 1): ?>
										<p style="margin-bottom: 0;"><b>Rent Until</b> : <?=$value['rent_deadline']?></p>
									<?php else:?>
										<p style="margin-bottom: 0;"><b>Not Available Until</b> : <?=$value['rent_deadline']?> </p>
									<?php endif;?>
								<?php endif;?>
							</div>

							<div class="pp_footer">
								<h5>Price: ₱<?=$value['selling_price']?></h5><br/>
								<?php if($value['availability'] == 1): ?>
									<a class="main_btn" href="<?=base_url()?>Client/Property/Item/<?=$value['pid']?>">
										<?php if($value['sale_type'] == 1): ?>
											For Rent
										<?php elseif($value['sale_type'] == 2): ?>
											For Transient
										<?php else: ?>
											For Sale
										<?php endif; ?>
									</a>
								<?php endif;?>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
		<?php if($page_data['pages'] > 1):?>
			<div class="pagination_area">
				<div class="back_holder">
					<div class="controls page_control first_page">
						<i class="fa fa-angle-double-left"></i>
					</div>
					<div class="controls page_control prev_page">
						<i class="fa fa-angle-left"></i>
					</div>
					<div class="current_page client_prop_page">
						<input type="number" class="active_page" value="1">
						<span style="margin: auto 0 auto 5px;">of </span>
						<span class="total_page" style="margin: auto 5px;"><?=$page_data['pages']?></span>
					</div>
					<div class="controls page_control next_page">
						<i class="fa fa-angle-right"></i>
					</div>
					<div class="controls page_control last_page">
						<i class="fa fa-angle-double-right"></i>
					</div>
				</div>
				<div style="width: 100%">
				</div>
			</div>
		<?php endif;?>
	</div>
</section>

<style type="text/css">
	.properties_area .col-sm-4{
		margin-bottom: 30px;
	}
</style>