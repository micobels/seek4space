<section class="banner_area">
	<div class="container-fluid">
		<div class="container">
			<div class="col-sm-8">
				<div class="card card-user" style="padding: 20px;">
					<h2 style="color: #575757;text-shadow: -3px 3px 10px #d3d3d3;">Thank you for your Request! <b><i class="fa fa-thumbs-up"></i></b></h2>
					<p>A Request Message was sent to the Seller. Please Check your Email or Wait for a Text Message for a Scheduled Meeting with the Seller.</p>
					<p>You can Either:</p>
					<ul class="options">
						<li>
							<i class="fa fa-file"></i>
							<span>Download Files and Start filling it Up. Present those files to the seller on your meeting Date.</span>
						</li>
						<li>
							<i class="fa fa-dollar" style="padding: 9px 12px"></i>
							<span>Pay Online or Via Bank for the Said Property. ( We advise that you push through on this process once all details are finilized with the Seller. )</span>
						</li>
					</ul>
					<div style="display: flex;">
						<button class="main_btn payment_btn" style="margin: 50px 5px 0;width: 100%;">Pay Online</button>
						<button class="second_btn export_documents" style="margin: 50px 5px 0;width: 100%;">Download Documents</button>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="card card-user">
	                <div class="image">
	                    <img src="<?=base_url().json_decode($details['images'])[0]?>" alt="..."/>
	                </div>
	                <?php if(!empty($seller)):?>
	                	<div class="content">
		                    <div class="author">
		                      <img class="avatar border-white" src="<?=base_url()?>assets/img/avatar.jpg" alt="..."/>
		                      <h4 class="title"><?=$details['property_name']?><br />
		                      		<a><small><?=$seller['fname'].' '.$seller['mname'].' '.$seller['lname']?></small></a>
		                      </h4>
		                    </div>
		                    <p class="description text-center">
		                        Location: <?=$details['property_location']?><br />
		                        Price: <?=$details['selling_price']?><br />
		                        Contact Number: <?=isset($seller['contact_number']) ? $seller['contact_number'] : '';?> 
		                    </p>
		                </div>
		                <hr>
		                <div class="text-center">
		                    <div class="row">
		                        <div class="col-md-3 col-md-offset-1">
		                            <h5><a href="<?=$seller['fb_link']?>"><i class="fa fa-facebook"></i></a><br /><small>Facebook</small></h5>
		                        </div>
		                        <div class="col-md-4">
		                            <h5><a href="<?=$seller['insta_link']?>"><i class="fa fa-instagram"></i></a><br /><small>Instagram</small></h5>
		                        </div>
		                        <div class="col-md-3">
		                            <h5><a href="mailto:<?=$seller['email']?>?&subject=Seek4Space Personal Contact&body="><i class="fa fa-envelope-o"></i></a><br /><small>Email</small></h5>
		                        </div>
		                    </div>
		                </div>
		             <?php else:?>
		             	<div class="content">
		             		<p class="description text-center">
		             			Not Available
		                    </p>
		             	</div>
	                <?php endif;?>
	            </div>
			</div>
		</div>
	</div>
</section>

<div id="property_payment" style="max-height: 600px;">
	<div class="initial_view" style="padding: 20px">
		<h3 style="margin: 0 0 20px;">Amount to be Paid: <b><?=$details['selling_price']?></b></h3>
		<p>Accredited Banks:</p>
		<ul class="list-inline">
			<li>
				<div class="img_holder">
					<img src="<?=base_url()?>assets/img/bank/bdo.png">
				</div>
				<p>0123465678954</p>
			</li>
			<li>
				<div class="img_holder">
					<img src="<?=base_url()?>assets/img/bank/bpi.png">
				</div>
				<p>0123465678954</p>
			</li>
			<li>
				<div class="img_holder">
					<img src="<?=base_url()?>assets/img/bank/metro.jpg">
				</div>
				<p>0123465678954</p>
			</li>
		</ul>
		<p style="margin-bottom: 30px;">Note: Please Pay from Credited Banks Only. Once Done, please Upload your Receipt Here to continue with the purchase of the Property. Wait for Seller Confirmation and Scheduled Meeting for other Details. Thank You!</p>
		<p class="notif_upload" style="color: red;display: none;">* Please Upload an Image of your Receipt.</p>
		<div class="upload_content">
			<div class="col-sm-6" style="padding-top: 50px;">
				<form method="POST" action="#" enctype="multipart/form-data" id="property_receipt_serialize">
					<div class="form-group">
                        <label class="btn second_btn" for="property_receipt">
                            Upload Receipt
                            <input type="file" name="property_receipt[]" id="property_receipt" style="display: none;">
                        </label>
                    </div>
					<button class="main_btn submit_receipt" type="submit" data-id="<?=$details['pid']?>">Confirm Payment</button>
				</form>
			</div>
			<div class="col-sm-6">
				<div class="reciept_image_container">
				</div>
			</div>
		</div>
	</div>
	<div class="success_view" style="text-align: center;display: none;padding: 50px 20px;">
		<i class="fa fa-check" style="font-size: 50px;color: #2d612d;"></i>
		<h2 style="color: #2d612d;">Payment Receipt Upload Successfull!</h2>
		<p>Note: Please Wait for Seller / Admin Confirmation. Always check your email or answer call from the seller for them to set an appointment regarding on the final contract and other paper details. <b>Thank you for your Purchase.</b></p>
	</div>
</div>

<style type="text/css">
	.banner_area{
		position: relative;
	    /*min-height: 754px;*/
	    padding: 50px;
	    background: url(../../../assets/img/banner/request.png) no-repeat scroll center center;
	    background-size: cover;
	}
	.card-user{
		box-shadow: -5px 5px 20px 0px #000000;
	}
	.options li{
		list-style: none;
		display: flex;
		margin-bottom: 20px;
	}
	.options li i{
		padding: 9px 10px;
	    font-size: 10px;
	    background: #575757;
	    border-radius: 50px;
	    color: #fff;
	    margin: auto 20px auto 0;
	}
	#property_payment ul{
		display: flex;
	}
	#property_payment li{
		width: 100%;
		padding: 10px;
		margin: 10px;
		border: solid 1px #d3d3d3;
		text-align: center;
		border-radius: 5px;
	}
	#property_payment li .img_holder{
		display: flex;
		height: 100px;
	}
	#property_payment li img{
		width: 100%;
		max-height: 90px;
		margin: auto;
	}
	#property_payment .upload_content{
		margin-bottom: 30px;
		display: inline-block;
		width: 100%;
		display: flex;
	}
	#property_payment .upload_content button,
	#property_payment .upload_content label{
		width: 90%;
		margin: auto;
		display: block;
	}
	.reciept_image_container{
		height: 200px;
		width: 80%;
		margin: auto;
		border: dotted 2px #d3d3d3;
		border-radius: 5px;
	}
	.reciept_image_container img{
		width: 95%;
		margin: auto;
		display: block;
		height: 195px;
	}
</style>