<section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
    	<div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
		<div class="container">
			<div class="banner_content" style="position: absolute;bottom: 30%;">
				<div class="page_link">
					<a href="<?=base_url()?>">Home</a>
					<a href="<?=base_url()?>Client/Properties">Properties</a>
					<a href="<?=base_url()?>Client/Property/Item/<?=$details['pid']?>"><?=$details['property_name']?></a>
				</div>
				<h2><?=$details['property_name']?></h2>
			</div>
		</div>
    </div>
</section>

<section class="property_images">
	<div class="container">
		<div class="center">
			<?php foreach (json_decode($details['images']) as $key => $value) : ?>
			<div>
				<div class="img_container">
					<img src="<?=base_url().$value?>">
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>

<section class="property_description">
	<div class="container">
		<div class="col-sm-8">
			<p><b>
				Property For 
				<?php 
				if($details['sale_type'] == 1){
					echo 'Rent';
				}elseif ($details['sale_type'] == 2) {
					echo 'Transient';
				}else{
					echo "Sale";
				}
				?>
			</b></p>
			<h3><?=$details['property_name']?></h3>
			<p class="main_description"><b><i class="fa fa-map-pin"></i> Location: </b> <?=$details['property_location']?></p>
			<p class="main_description"><b>₱ Price: </b> <?=$details['selling_price']?></p>
			<p class="main_description"><b>Description: </b> <?=$details['description']?></p>
			<p class="note"><b>*Note :</b> All Properties have been tested and made sure that the sellers are trusted and the properties being posted are accurate and true.</p>
			<?php if(isset($user)): ?>
			<?php if($user['user_type'] == 1) : ?>
			<button class="main_btn redirect_request" data-id="<?=$details['pid']?>" style="margin-top: 50px;"><?php 
				if($details['sale_type'] == 1){
					echo 'Rent';
				}elseif ($details['sale_type'] == 2) {
					echo 'Book';
				}else{
					echo "Buy";
				}
				?> Property</button>
			<?php endif; ?>
			<?php else: ?>
				<button class="main_btn login_first">
					<?php 
					if($details['sale_type'] == 1){
						echo 'Rent';
					}elseif ($details['sale_type'] == 2) {
						echo 'Book';
					}else{
						echo "Buy";
					}
					?> Property
				</button>
			<?php endif;?>
		</div>
		<div class="col-sm-4">
            <?php if(!empty($seller)):?>
				<div class="card card-user">
	                <div class="image">
	                    <img src="<?=base_url()?>assets/img/background.jpg" alt="..."/>
	                </div>
	                <div class="content">
	                    <div class="author">
	                      <img class="avatar border-white" src="<?=base_url()?>assets/img/avatar.jpg" alt="..."/>
	                      	<h4 class="title"><?=$seller['fname'].' '.$seller['mname'].' '.$seller['lname']?><br />
	                      		<a><small><?=$seller['contact_number']?></small></a>
	                      	</h4>
	                    </div>
	                    <p class="description text-center">
	                        "<?=$seller['about']?>"
	                    </p>
	                </div>
	                <hr>
	                <div class="text-center">
	                    <div class="row">
	                        <div class="col-md-3 col-md-offset-1">
	                            <h5><a href="<?=$seller['fb_link']?>"><i class="fa fa-facebook"></i></a><br /><small>Facebook</small></h5>
	                        </div>
	                        <div class="col-md-4">
	                            <h5><a href="<?=$seller['insta_link']?>"><i class="fa fa-instagram"></i></a><br /><small>Instagram</small></h5>
	                        </div>
	                        <div class="col-md-3">
	                            <h5><a href="mailto:<?=$seller['email']?>?&subject=Seek4Space Personal Contact&body="><i class="fa fa-envelope-o"></i></a><br /><small>Email</small></h5>
	                        </div>
	                    </div>
	                </div>
	            </div>
        	<?php endif;?>
		</div>
	</div>
</section>

<section class="map_section" style="margin: 50px 0;">
	<div class="container">
		<?=$details['map']?>
	</div>
</section>

<section class="properties_area" style="margin-top: 50px;">
	<div class="container">
		<div class="main_title">
			<h2>Related Properties that you Might Like</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</p>
		</div>
		<div class="row properties_inner">
			<?php foreach ($related as $key => $value) : ?>
			<div class="col-sm-4">
				<div class="properties_item">
					<div class="pp_img">
						<img class="img-fluid" src="<?=base_url().json_decode($value['images'])[0]?>" style="width: 100%;border-radius: 5px;height: 250px;">
					</div>
					<div class="pp_content">
						<a href="#"><h4><?=$value['property_name']?></h4></a>
						<div class="location">
                            <p><b>Location:</b> <?=$value['property_location']?></p>
						</div>
						<div class="pp_footer">
							<h5>Price: ₱<?=$value['selling_price']?></h5>
							<a class="main_btn">
								<?php if($value['sale_type'] == 1): ?>
									For Rent
								<?php elseif($value['sale_type'] == 2): ?>
									For Transient
								<?php else: ?>
									For Sale
								<?php endif; ?>
							</a>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>

<div id="login_modal" style="max-height: 600px;text-align: center;">
	<div style="padding: 50px 20px;">
		<h2><b>Please Login!</b></h2>
		<p>Please Login first for you to be able to access some Pages.</p>
		<a href="<?=base_url()?>Login">
			<button class="main_btn">Login</button>
		</a>
		<a href="<?=base_url()?>Register">
			<button class="second_btn">Sign Up</button>
		</a>
	</div>
</div>

<style type="text/css">
	iframe{
		width: 100%;
	}
	.property_images{
		margin: 50px 0;
	}
	.property_images .img_container{
		margin: 0 5px;
		border: solid 3px #d3d3d3;
	}
	.property_images .img_container img{
		width: 100%;
		height: 200px;
	}
	.property_images .slick-arrow{
	    position: absolute;
	    background: #fff;
	    border: none;
	    z-index: 3;
	    width: 55px;
	    height: 55px;
	    text-align: center;
	    font-size: 50px;
	    border-radius: 50%;
	    color: #105a9c;
	    box-shadow: 0px 0px 10px 2px;
	}
	.property_images .slick-prev{
		top: 50%;
	    left: 20%;
	    transform: translate(-50%,-50%);
	}
	.property_images .slick-next{
		top: 50%;
	    right: 14.5%;
	    transform: translate(-50%,-50%);
	}
	.property_description p{
		margin: 0;
	}
	.property_description h3{
		margin: 0 0 30px;
		font-weight: 1000;
		font-size: 30px;
		color: #444;
	}
	.property_description .main_description{
		font-size: 16px;
		font-weight: 500;
		margin-top: 5px;
	}
	.property_description .note{
		margin-top: 50px;
	}
</style>