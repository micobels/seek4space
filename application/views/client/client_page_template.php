<?php if($page_label == 'perperty_list'):?>
	
	<?php foreach ($properties as $key => $value): ?>
		<div class="col-sm-4">
			<div class="properties_item">
				<div class="pp_img">
					<img class="img-fluid" src="<?=base_url().json_decode($value['images'])[0]?>" style="width: 100%;border-radius: 5px;height: 250px;">
				</div>
				<div class="pp_content">
					<a href="#"><h4><?=$value['property_name']?></h4></a>
					<div class="location">
						<p><b>Location:</b> <?=$value['property_location']?></p>
					</div>
					<div class="deadline">
						<?php if($value['sale_type'] == 1): ?>
							<?php if($value['availability'] == 1): ?>
								<p style="margin-bottom: 0;"><b>Rent Until</b> : <?=$value['rent_deadline']?></p>
							<?php else:?>
								<p style="margin-bottom: 0;"><b>Not Available Until</b> : <?=$value['rent_deadline']?> </p>
							<?php endif;?>
						<?php endif;?>
					</div>
					<div class="pp_footer">
						<h5>Price: ₱<?=$value['selling_price']?></h5>
						<?php if($value['availability'] == 1): ?>
							<a class="main_btn" href="<?=base_url()?>Client/Property/Item/<?=$value['pid']?>">
								<?php if($value['sale_type'] == 1): ?>
									For Rent
								<?php elseif($value['sale_type'] == 2): ?>
									For Transient
								<?php else: ?>
									For Sale
								<?php endif; ?>
							</a>
						<?php endif;?>
					</div>
				</div>
			</div>
		</div>
	<?php endforeach; ?>

<?php endif;?> <!-- end if $page_label -->