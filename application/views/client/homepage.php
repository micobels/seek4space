<!--================Home Banner Area =================-->
<section class="home_banner_area" style="margin-top: 50px;">
    <div class="banner_inner d-flex align-items-center">
		<div class="container">
			<div class="banner_content">
				<h5>Let us Guide you Home</h5>
				<h3>Seek4Space</h3>
				<a class="main_btn" href="<?=base_url()?>Client/About_us">Learn More</a>
			</div>
		</div>
    </div>
    <!-- <div class="container">
		<div class="advanced_search">
			<h3>Search Properties for</h3>
			<div class="search_select">
				<input type="text" name="Name" placeholder="Property Name" class="main_input">
                <input type="text" name="Location" placeholder="Property Location" class="main_input">
                <select class="s_select">
                    <option value="0">Choose Sale Type</option>
                    <option value="1">Type 1</option>
                    <option value="2">Type 2</option>
                </select>
                <input type="number" name="min_pirce" placeholder="Min. Price" class="main_input">
                <input type="number" name="max_price" placeholder="Max. Price" class="main_input">
			</div>
			<button type="submit" value="submit" class="btn submit_btn">Search Property</button>
		</div>
    </div> -->
</section>
<!--================End Home Banner Area =================-->

<!--================Welcome Area =================-->
<section class="welcome_area p_120">
	<div class="container">
		<div class="row welcome_inner">
			<div class="col-lg-6">
				<div class="welcome_img">
					<img class="img-fluid" src="<?=base_url();?>assets/img/welcome-1.jpg" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<div class="welcome_text">
					<h4>Welcome to <b>Seek4Space</b></h4>
					<p>inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct standards especially in the workplace. That’s why it’s crucial that, as women, our behavior on the job is beyond reproach. inappropriate behavior is often laughed.</p>
					<div class="row">
						<div class="col-sm-4">
							<div class="wel_item">
								<i class="lnr lnr-database"></i>
								<h4>₱2.5M</h4>
								<p>Total Sales</p>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="wel_item">
								<i class="lnr lnr-book"></i>
								<h4>1465</h4>
								<p>Total Properties</p>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="wel_item">
								<i class="lnr lnr-users"></i>
								<h4>3965</h4>
								<p>Total Clients</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--================End Welcome Area =================-->

<!--================Properties Area =================-->
<section class="properties_area">
	<div class="container">
		<div class="main_title">
			<h2>Our Newest Properties</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</p>
		</div>
		<div class="row properties_inner">
			<?php foreach ($newest as $key => $value): ?>
			<div class="col-sm-4">
				<div class="properties_item">
					<div class="pp_img">
						<img class="img-fluid" src="<?=base_url().json_decode($value['images'])[0]?>" style="width: 100%;border-radius: 5px;height: 250px;">
					</div>
					<div class="pp_content">
						<a href="#"><h4><?=$value['property_name']?></h4></a>
						<div class="location">
                            <p><b>Location:</b> <?=$value['property_location']?></p>
						</div>
						<div class="pp_footer">
							<h5>Price: ₱<?=$value['selling_price']?></h5>
							<a class="main_btn" href="<?=base_url()?>Client/Property/Item/<?=$value['pid']?>">
								<?php if($value['sale_type'] == 1): ?>
									For Rent
								<?php elseif($value['sale_type'] == 2): ?>
									For Transient
								<?php else: ?>
									For Sale
								<?php endif; ?>
							</a>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>
<!--================End Properties Area =================-->

<!--================Testimonials Area =================-->
<section class="testimonials_area p_120">
	<div class="container">
		<div class="row testimonials_inner">
			<div class="col-lg-4">
				<div class="testi_left_text">
					<h4>Client’s Feedback</h4>
					<p>inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct standards especially in the workplace. That’s why it’s crucial that, as women, our behavior on the job is beyond reproach. inappropriate behavior is often laughed.</p>
				</div>
			</div>
			<div class="col-lg-8">
				<div class="col-sm-6">
                    <div class="testi_item">
                        <img src="<?=base_url()?>assets/img/testimonials/testi-1.png">
                        <p>We all live in an age that belongs to the young at heart. Life that is becoming extremely fast, day to day,</p>
                        <h4>Jane Doe</h4>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="testi_item">
                        <img src="<?=base_url()?>assets/img/testimonials/testi-2.png">
                        <p>We all live in an age that belongs to the young at heart. Life that is becoming extremely fast, day to day,</p>
                        <h4>John Doe</h4>
                    </div>
                </div>
			</div>
		</div>
	</div>
</section>
<!--================End Testimonials Area =================-->

<!--================Cities Area =================-->
<section class="cities_area p_120">
	<div class="container">
		<div class="main_title">
			<h2>More of what we can Offer</h2>
			<p>We all live in an age that belongs to the young at heart. Life that is becoming extremely fast, day to day,</p>
		</div>
		<div class="row cities_inner">
			<div class="col-lg-4 col-md-4">
				<div class="cities_item">
					<img class="img-fluid" src="<?=base_url();?>assets/img/cities/rent.jpg" alt="">
					<a class="main_btn2 browse_properties" data-type="1">Rent Now</a>
				</div>
			</div>
			<div class="col-lg-4 col-md-4">
				<div class="cities_item">
					<img class="img-fluid" src="<?=base_url();?>assets/img/cities/transient.jpg" alt="">
					<a class="main_btn2 browse_properties" data-type="2">Book Now</a>
				</div>
			</div>
			<div class="col-lg-4 col-md-4">
				<div class="cities_item">
					<img class="img-fluid" src="<?=base_url();?>assets/img/cities/house.jpg" alt="">
					<a class="main_btn2 browse_properties" data-type="3">Buy Now</a>
				</div>
			</div>
		</div>
	</div>
</section>
<!--================End Cities Area =================-->

<!--================Feature Area =================-->
<section class="feature_area p_120">
	<div class="container">
		<div class="main_title">
			<h2>Why we are the best</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
		</div>
		<div class="row feature_inner">
			<div class="col-lg-4 col-md-6">
				<div class="feature_item">
					<h4><i class="lnr lnr-user"></i>Expert Technicians</h4>
					<p>Usage of the Internet is becoming more common due to rapid advancement of technology and power.</p>
				</div>
			</div>
			<div class="col-lg-4 col-md-6">
				<div class="feature_item">
					<h4><i class="lnr lnr-license"></i>Professional Service</h4>
					<p>Usage of the Internet is becoming more common due to rapid advancement of technology and power.</p>
				</div>
			</div>
			<div class="col-lg-4 col-md-6">
				<div class="feature_item">
					<h4><i class="lnr lnr-phone"></i>Great Support</h4>
					<p>Usage of the Internet is becoming more common due to rapid advancement of technology and power.</p>
				</div>
			</div>
			<div class="col-lg-4 col-md-6">
				<div class="feature_item">
					<h4><i class="lnr lnr-rocket"></i>Technical Skills</h4>
					<p>Usage of the Internet is becoming more common due to rapid advancement of technology and power.</p>
				</div>
			</div>
			<div class="col-lg-4 col-md-6">
				<div class="feature_item">
					<h4><i class="lnr lnr-diamond"></i>Highly Recomended</h4>
					<p>Usage of the Internet is becoming more common due to rapid advancement of technology and power.</p>
				</div>
			</div>
			<div class="col-lg-4 col-md-6">
				<div class="feature_item">
					<h4><i class="lnr lnr-bubble"></i>Positive Reviews</h4>
					<p>Usage of the Internet is becoming more common due to rapid advancement of technology and power.</p>
				</div>
			</div>
		</div>
	</div>
</section>
<!--================End Feature Area =================-->


<!--================Sell a Property Area =================-->
<section class="sell_a_property p_120">
	<div class="container">
		<div class="main_title">
			<h2 style="color: orange;">Sell a Property?</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris</p>
			<a href="<?=base_url()?>Register">
            	<button class="second_btn" style="margin-top: 20px;">Start Selling</button>
            </a>
		</div>
	</div>
</section>
<!--================End Sell a Property Area =================-->
<style type="text/css">
	.cities_item img{
		width: 100%;
	    border-radius: 5px;
	    height: 300px;
	    object-fit: cover;
	}
</style>