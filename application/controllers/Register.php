<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->scripts = [
			base_url().'/assets/js/auth/authentication.js',
			base_url().'assets/js/iziModal.min.js'
		];
		$this->load->model('Common/Common_model', 'common');
	}

	public function index(){
		$data             = [];
		$data['js_array'] = $this->scripts;
		$data['page_title'] = 'Registration';
		$this->template->set_layout('layout/default_web', $data);
		$this->template->initPartials('header','web_header');
		$this->template->initPartials('footer','web_footer');
		$this->template->buildPage('layout/register',$data);
	}

	public function process_registration(){
		$params   = $this->input->post('content');
		$salt     = dechex(mt_rand(0, 2147483647)) . dechex(mt_rand(0, 2147483647));
		$password = hash('sha256', $params['password'] . $salt);
		for($round = 0; $round < 65536; $round++){
			$password = hash('sha256', $password);
		}
		$user_data = array(
			'user_type'      => $params['utype'],
			'email'          => $params['email'],
			'password'       => $password,
			'salt'           => $salt,
			'fname'          => $params['fname'],
			'mname'          => $params['mname'],
			'lname'          => $params['lname'],
			// 'age'            => $params['age'],
			'birth_date'     => $params['bday'],
			'birth_place'    => $params['bplace'],
			'address'        => $params['address'],
			'civil_status'   => $params['civil_stat'],
			'gender'         => $params['gender'],
			'nationality'    => $params['nationality'],
			// 'citizenship'    => $params['citizenship'],
			// 'no_dependents'  => $params['dependents'],
			'postal_code'    => $params['postal_code'],
			'contact_number' => $params['contact_no'],
			'bank_account'   => $params['bank_account'],
			'about'          => $params['about'],
			'fb_link'        => $params['facebook'],
			'insta_link'     => $params['intagram'],
		);

		$saved = $this->common->ss_insert('ss_accounts', $user_data);

		if($params['utype'] == 1){
			$this->common->ss_insert('ss_accounts_buyers', $user_data);
		}elseif($params['utype'] == 2){
			$this->common->ss_insert('ss_accounts_sellers', $user_data);
		}
		if($saved){
			json_response('success', 'saved account', []);
		}else{
			json_response('failed', 'something went wrong', []);
		}
	}
}
