<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Properties extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Common/Common_model', 'common');
		$this->scripts = [
			base_url().'assets/js/admin/properties.js',
			base_url().'assets/js/iziModal.min.js'
		];
		$this->limit = 9;
	}

	public function index(){
		$data = [];
		$data['js_array'] = $this->scripts;
		$data['page_title'] = 'Properties';
		$user = $this->session->userdata('logged_in');
		$where = $user['user_type'] == 0 ? [] : ['creator' => $user['user_id']];
		$data['user'] = $this->session->userdata('logged_in');

		$data['properties'] = $this->common->ss_table_data('ss_properties', '*',$where,['pid' =>'DESC'],['per_page' => $this->limit, 'segment' => 0]);
		$total_list = $this->common->ss_table_data('ss_properties',['count(pid) as total'],$where)[0]['total'];
		$data['pages'] = get_pages($total_list, $this->limit);
		
		$this->template->set_layout('layout/default_admin');
		$this->template->initPartials('header','main_header');
		$this->template->initPartials('footer','main_footer');
		$this->template->buildPage('admin/properties', $data);
	}

	public function delete_property(){
		$id = $this->input->post('data_id');
		$this->common->ss_delete('ss_properties',['pid' => $id]);
		json_response('success','Property Successfully Deleted.');
	}

	public function load_table(){
		$curr_page = $this->input->post('page');
		$search = $this->input->post('search');
		$offset = ($curr_page-1)*$this->limit;
		$data['table'] = 'properties';
		$user = $this->session->userdata('logged_in');
		$where = $user['user_type'] == 0 ? [] : ['creator' => $user['user_type']];
		// print_r($search);
		$search_data = [];
		if($search['keyword'] != ''){
			$search_data['property_name'] = $search['keyword'];
			$search_data['property_location'] = $search['keyword'];
			$search_data['description'] = $search['keyword'];
		}

		if($search['min'] != ''){
			$where['selling_price >= '] = (int)$search['min'];
		}

		if($search['max'] != ''){
			$where['selling_price <= '] = (int)$search['max'];
		}

		$data['properties'] = $this->common->ss_table_data('ss_properties', '*',$where,['pid' =>'DESC'],['per_page' => $this->limit, 'segment' => $offset], $search_data);

      	$this->load->view('admin/admin_dashboard_template',$data);
	}
}
