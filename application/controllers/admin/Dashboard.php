<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct(){
		parent::__construct();
	}

	public function index(){
		$data = [];
		$data['user'] = $this->session->userdata('logged_in');
		$data['page_title'] = 'Dashboard';
		$this->template->set_layout('layout/default_admin');
		$this->template->initPartials('header','main_header');
		$this->template->initPartials('footer','main_footer');
		$this->template->buildPage('admin/dashboard',$data);
	}
}
