<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Add_property extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Common/Common_model', 'common');
		$this->scripts = [
			base_url().'/assets/js/admin/add_property.js',
			base_url().'assets/js/iziModal.min.js'
		];
	}

	public function index(){
		$data             = [];
		$data['js_array'] = $this->scripts;
		$data['page_title'] = 'Add New Property';
		$data['user'] = $this->session->userdata('logged_in');
		
		$this->template->set_layout('layout/default_admin',$data);
		$this->template->initPartials('header','main_header',$data);
		$this->template->initPartials('footer','main_footer');
		$this->template->buildPage('admin/create_properties', $data);
	}

	public function save_property(){
		extract($_POST);
		if($_FILES){
			if($_FILES['property_images']){
				$property_images       = $_FILES['property_images'];
				$property_images_total = count($_FILES['property_images']['name']);
				$property_images_saved = [];
				for( $i=0 ; $i < $property_images_total ; $i++ ) {
					$temp_file_path = $property_images['tmp_name'][$i];

					if ($temp_file_path != "" && $name != ''){
						$final_file_name  = $name . $i;
						$extension        = explode("/", $property_images["type"][$i]);
						$upload_dir_image = FCPATH . 'assets/img/properties/'.str_replace(" ", "-", $name)."/area";

						if (!file_exists($upload_dir_image)) {
							mkdir($upload_dir_image, 0755, true);
						}

						$newFilePath = "assets/img/properties/".str_replace(" ", "-", $name)."/area/". str_replace(" ", "-", $final_file_name).".".$extension[1];
						array_push($property_images_saved, $newFilePath);

						if(move_uploaded_file($temp_file_path, $newFilePath)) {
							chmod(FCPATH .$newFilePath, 0755);
						}
					}
				}
			}

			if($_FILES['legal_document']){
				$legal_document       = $_FILES['legal_document'];
				$legal_document_total = count($_FILES['legal_document']['name']);
				$legal_document_saved = [];
				for( $i=0 ; $i < $legal_document_total ; $i++ ) {
					$temp_file_path = $legal_document['tmp_name'][$i];

					if ($temp_file_path != "" && $name != ''){
						$final_file_name  = $name . $i;
						$extension        = explode("/", $legal_document["type"][$i]);
						$upload_dir_image = FCPATH . 'assets/img/properties/'.str_replace(" ", "-", $name)."/docs";

						if (!file_exists($upload_dir_image)) {
							mkdir($upload_dir_image, 0755, true);
						}

						$newFilePath = "assets/img/properties/".str_replace(" ", "-", $name)."/docs/". str_replace(" ", "-", $final_file_name).".".$extension[1];
						array_push($legal_document_saved, $newFilePath);

						if(move_uploaded_file($temp_file_path, $newFilePath)) {
							chmod(FCPATH .$newFilePath, 0755);
						}
					}
				}
			}
		}

		$user = $this->session->userdata('logged_in');

		$property_data = array(
			'property_name'     => $name,
			'property_location' => $location,
			'sale_type'         => $type,
			'original_price'    => 0,
			'selling_price'     => $sell_price,
			'images'            => json_encode($property_images_saved),
			'legal_docs'        => json_encode($legal_document_saved),
			'map'				=> $embed,
			'description'       => $about,
			'creator' 			=> $user['user_id'],
			'rent_deadline' 	=> $rent_date,
		);
		
		$this->common->ss_insert('ss_properties', $property_data);
		json_response('success', 'saved prop', []);
	}
}
