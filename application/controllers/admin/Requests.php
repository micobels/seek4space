<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Requests extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Common/Common_model', 'common');
		$this->scripts = [
			base_url().'/assets/js/admin/requests.js',
			base_url().'assets/js/iziModal.min.js'
		];
		$this->limit = 10;
	}

	public function index(){
		$data             = [];
		$data['js_array'] = $this->scripts;
		$data['page_title'] = 'Add New Property';
		$data['user'] = $this->session->userdata('logged_in');
		$where = [];
		if($data['user']['user_type'] != 0){
			$where['req.property_id'] = $this->get_prop_by_user();
		}

		$data['list'] = $this->common->get_requests([],$where,['per_page' => $this->limit,'segment' => 0]);
		$total_list = $this->common->get_requests(['count(req.id) as total'],$where)[0]['total'];
		$data['pages']      = get_pages($total_list, $this->limit);
		
		$this->template->set_layout('layout/default_admin');
		$this->template->initPartials('header','main_header');
		$this->template->initPartials('footer','main_footer');
		$this->template->buildPage('admin/requests', $data);
	}

	public function get_prop_by_user(){
		$user = $this->session->userdata('logged_in');
		$data_id = $this->common->ss_table_data('ss_properties',['pid', 'availability'],['creator' => $user['user_id']]);
		$ids = [];
		foreach ($data_id as $key => $value) {
			array_push($ids, $value['pid']);
		}
		return $ids;
	}

	public function load_table(){
		$curr_page = $this->input->post('page');
		$offset = ($curr_page-1)*$this->limit;
		$data['table'] = 'requests';
		$user = $this->session->userdata('logged_in');
		$where = [];
		if($user['user_type'] != 0){
			$where['req.property_id'] = $this->get_prop_by_user();
		}

		$data['list'] = $this->common->get_requests([],$where,['per_page' => $this->limit,'segment' => $offset]);

		$this->load->view('admin/admin_dashboard_template',$data);
	}

	public function change_status(){
		$id = $this->input->post('id');
		$status = $this->input->post('status');
		$property = $this->input->post('property');

		$this->common->ss_update('ss_requests',['status' => $status],['id' => $id]);

		if($status == 3 && $property > 0){
			$this->common->ss_update('ss_properties',['status' => 0],['pid' => $property]);
		}
		json_response('success','');
	}

	public function delete_request(){
		$id = $this->input->post('data_id');

		$this->common->ss_delete('ss_requests',['id' => $id]);
		json_response('success','');
	}

	public function mask_rented(){
		$id = $this->input->post('data_id');
		$val = $this->input->post('val');
		if($val == 0){
			$availability = 1;
		}else{
			$availability = 0;
		}
		$this->common->ss_update('ss_properties',['availability' => $availability],['pid' => $id]);
		json_response('success','');
	}
}
?>
