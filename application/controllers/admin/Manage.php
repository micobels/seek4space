<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Common/Common_model', 'common');
		$this->scripts = [
			base_url().'/assets/js/admin/properties.js',
			base_url().'assets/js/iziModal.min.js'
		];
	}

	public function Property($property_id){
		$data = [];
		$data['user'] = $this->session->userdata('logged_in');
		$data['js_array'] = $this->scripts;
		$data['page_title'] = 'Manage Property';

		$data['list'] = $this->common->ss_table_data('ss_properties',['pid','property_name','property_location','original_price','selling_price','sale_type','images','description','status'],['pid' => $property_id])[0];

		$this->template->set_layout('layout/default_admin',$data);
		$this->template->initPartials('header','main_header');
		$this->template->initPartials('footer','main_footer');
		$this->template->buildPage('admin/manage',$data);
	}

	public function Update_property(){
		$name = $this->input->post('name');
		$status = $this->input->post('status');
		$address = $this->input->post('address');
		$orig = $this->input->post('orig');
		$selling = $this->input->post('selling');
		$id = $this->input->post('id');

		$this->common->ss_update('ss_properties',['property_name' => $name,'sale_type' => $status,'property_location' => $address,'original_price' => $orig,'selling_price'=> $selling],['pid' => $id]);
		json_response('success','Property Successfully Updated.');
	}

	public function activate_property(){
		$type = $this->input->post('type');
		$id = $this->input->post('id');
		$this->common->ss_update('ss_properties',['status' => $type],['pid' => $id]);
		json_response('success','Status Changed.');
	}

	public function get_legal_documents(){
		$id = $this->input->post('id');
		$images = $this->common->ss_table_data('ss_properties','legal_docs',['pid' => $id])[0];
		$images = json_decode($images['legal_docs']);
		json_response('','',$images);
	}
}
