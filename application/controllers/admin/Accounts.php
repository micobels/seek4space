<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounts extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Common/Common_model', 'common');
		$this->scripts = [
			base_url().'/assets/js/admin/accounts.js',
			base_url().'assets/js/iziModal.min.js'
		];
		$this->limit = 10;
	}

	public function index(){
		if($this->session->userdata('logged_in') && $this->session->userdata('logged_in')['user_type'] == 0){
			$data = [];
			$data['js_array'] = $this->scripts;
			$data['page_title'] = 'Accounts';
			$data['user'] = $this->session->userdata('logged_in');

			$data['list'] = $this->common->ss_table_data('ss_accounts',['user_id','user_type','email','fname','lname','mname','contact_number'],[],['user_id' =>'DESC'],['per_page' => $this->limit, 'segment' => 0]);
			$total_list = $this->common->ss_table_data('ss_accounts',['count(user_id) as total'])[0]['total'];
			$data['pages']      = get_pages($total_list, $this->limit);

			$this->template->set_layout('layout/default_admin',$data);
			$this->template->initPartials('header','main_header');
			$this->template->initPartials('footer','main_footer');
			$this->template->buildPage('admin/accounts',$data);
		}else{
			redirect('Admin/Dashboard');
		}
	}

	public function Get_data(){
		$id = $this->input->post('id');

		$user_data = $this->common->ss_table_data('ss_accounts',[],['user_id' => $id])[0];
		json_response('','',$user_data);
	}

	public function Delete_account(){
		$id = $this->input->post('id');
		$this->common->ss_delete('ss_accounts',['user_id' => $id]);
		$this->common->ss_delete('ss_accounts_buyers',['user_id' => $id]);
		$this->common->ss_delete('ss_accounts_sellers',['user_id' => $id]);
		$this->common->ss_delete('ss_properties',['creator' => $id]);
		json_response('success','Account Deleted.');
	}

	public function load_table(){
		$curr_page = $this->input->post('page');
		$offset = ($curr_page-1)*$this->limit;
		$data['table'] = 'accounts';

		$data['list'] = $this->common->ss_table_data('ss_accounts',['user_id','user_type','email','fname','lname','mname','contact_number'],[],['user_id' =>'DESC'],['per_page' => $this->limit, 'segment' => $offset]);

      	$this->load->view('admin/admin_dashboard_template',$data);
	}
}
