<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Common/Common_model', 'common');
		$this->scripts = [
			base_url().'/assets/js/admin/profile.js',
			base_url().'assets/js/iziModal.min.js'
		];
	}

	public function index(){
		$data = [];
		$data['user'] = $this->session->userdata('logged_in');
		$data['page_title'] = 'Profile';
		$data['js_array'] = $this->scripts;
		$this->template->set_layout('layout/default_admin');
		$this->template->initPartials('header','main_header');
		$this->template->initPartials('footer','main_footer');
		$this->template->buildPage('admin/profile', $data);
	}

	public function Update_profile(){
		$data = $this->input->post('data');
		$new_data = [
			'email' => $data['email'],
			'fname' => $data['fname'],
			'mname' => $data['mname'],
			'lname' => $data['lname'],
			// 'age' => $data['age'],
			'birth_date' => $data['birth_date'],
			'birth_place' => $data['birth_place'],
			'address' => $data['address'],
			'civil_status' => $data['civil_status'],
			'gender' => $data['gender'],
			'nationality' => $data['nationality'],
			// 'citizenship' => $data['citizenship'],
			// 'no_dependents' => $data['dependents'],
			'postal_code' => $data['postal_code'],
			'contact_number' => $data['mobile'],
			'bank_account' => $data['bank_account'],
			'about' => $data['about'],
		];

		$this->common->ss_update('ss_accounts',$new_data,['user_id' => $data['id']]);
		$this->common->ss_update('ss_accounts_buyers',$new_data,['user_id' => $data['id']]);
		$this->common->ss_update('ss_accounts_sellers',$new_data,['user_id' => $data['id']]);
		json_response('success','profile updated.');
	}
}
