<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Common/Common_model', 'common');
		$this->scripts = [
			base_url().'/assets/js/web/home.js'
		];
	}

	public function index(){
		$data = [];
		$data['js_array'] = $this->scripts;
		$data['page_title'] = 'Home';
		if($this->session->userdata('logged_in')){
			$data['user'] = $this->session->userdata('logged_in');
		}

		$data['newest'] = $this->common->ss_table_data('ss_properties',['images','property_name','property_location','selling_price','sale_type','pid'],['status' => 1],['pid'=>'DESC'],['per_page'=>3,'segment'=>0]);

		$this->template->set_layout('layout/default_web');
		$this->template->initPartials('header','web_header');
		$this->template->initPartials('footer','web_footer');
		$this->template->buildPage('client/homepage',$data);
	}
}
