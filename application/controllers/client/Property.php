<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Property extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Common/Common_model', 'common');
		$this->scripts = [
			base_url().'/assets/js/web/properties.js',
			base_url().'assets/js/iziModal.min.js'
		];
	}

	public function item($id){
		if(isset($id)){
			$data['js_array'] = $this->scripts;
			$data['page_title'] = 'Property Item';
			if($this->session->userdata('logged_in')){
				$data['user'] = $this->session->userdata('logged_in');
			}

			$data['details'] = $this->common->ss_table_data('ss_properties',['images','property_name','property_location','selling_price','sale_type','pid','description','map','creator'],['pid' => $id]);
			$data['seller'] = $this->common->ss_table_data('ss_accounts',['user_id','fname','lname','mname','contact_number','email','about','fb_link','insta_link'],['user_id' => $data['details'][0]['creator']]);
			$data['related'] = $this->common->ss_table_data('ss_properties',['images','property_name','property_location','selling_price','sale_type','pid'],['status' => 1,'sale_type' => $data['details'][0]['sale_type'],'pid !=' =>$id],['pid'=>'RANDOM'],['per_page'=>3,'segment'=>0]);

			$data['details'] = !empty($data['details']) ? $data['details'][0] : [];
			$data['seller'] = !empty($data['seller']) ? $data['seller'][0] : [];

			$this->template->set_layout('layout/default_web');
			$this->template->initPartials('header','web_header');
			$this->template->initPartials('footer','web_footer');
			$this->template->buildPage('client/property',$data);
		}else{
			redirect('');
		}
	}

	public function set_request(){
		$id = $this->input->post('id');
		$user = $this->session->userdata('logged_in');
		$date = date('Y-m-d H:i:s');

		$checker = $this->common->ss_table_data('ss_requests',[],['user_id' => $user['user_id'],'property_id' => $id]);
		if(empty($checker) && $user['user_type'] == 1){
			$this->common->ss_insert('ss_requests',['user_id' => $user['user_id'],'property_id'=>$id,'date'=>$date]);
		}
		json_response($user['user_type'] == 1 ? 'success' : 'failed','');
	}

	public function request($id){
		if(isset($id) && $this->session->userdata('logged_in')){
			$data['js_array'] = $this->scripts;
			$data['page_title'] = 'Property Request';
			if($this->session->userdata('logged_in')){
				$data['user'] = $this->session->userdata('logged_in');
			}

			$data['details'] = $this->common->ss_table_data('ss_properties',['images','property_name','property_location','selling_price','sale_type','pid','description','map','creator'],['pid' => $id]);
			$data['seller'] = $this->common->ss_table_data('ss_accounts',['user_id','fname','lname','mname','contact_number','email','about','fb_link','insta_link'],['user_id' => $data['details'][0]['creator']]);

			$data['details'] = !empty($data['details']) ? $data['details'][0] : [];
			$data['seller'] = !empty($data['seller']) ? $data['seller'][0] : [];

			$this->template->set_layout('layout/default_web');
			$this->template->initPartials('header','web_header');
			$this->template->initPartials('footer','web_footer');
			$this->template->buildPage('client/request',$data);
		}else{
			redirect('');
		}
	}

	public function upload_payment(){
		extract($_POST);
		$user = $this->session->userdata('logged_in');

		if($_FILES){
			if($_FILES['property_receipt']){
				$property_receipt       = $_FILES['property_receipt'];
				$property_receipt_total = count($_FILES['property_receipt']['name']);
				$property_receipt_saved = [];
				for( $i=0 ; $i < $property_receipt_total ; $i++ ) {
					$temp_file_path = $property_receipt['tmp_name'][$i];

					if ($temp_file_path != "" && $id != ''){
						$final_file_name  = $id .'.'. $user['user_id'] .'.'. strtotime(date('Y-m-d H:i'));
						$extension        = explode("/", $property_receipt["type"][$i]);
						$upload_dir_image = FCPATH . 'assets/img/receipt/'.$id;

						if (!file_exists($upload_dir_image)) {
							mkdir($upload_dir_image, 0755, true);
						}

						$newFilePath = "assets/img/receipt/".$id.'/'.$final_file_name.".".$extension[1];
						array_push($property_receipt_saved, $newFilePath);

						if(move_uploaded_file($temp_file_path, $newFilePath)) {
							chmod(FCPATH .$newFilePath, 0755);
						}
					}
				}
			}
		}

		$new_data = array(
			'status' => 2,
			'date' => date('Y-m-d H:i'),
			'receipt' => json_encode($property_receipt_saved)
		);
		$this->common->ss_update('ss_requests',$new_data,['user_id' => $user['user_id'],'property_id' => $id]);
		json_response('success','');
	}

	public function download_folder(){
		$this->load->library('zip');
	    $path = FCPATH.'/assets/documents';
	    $this->zip->read_dir($path,FALSE);
	    $this->zip->download('documents.zip');
	}
}
