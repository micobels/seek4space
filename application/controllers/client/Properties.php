<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Properties extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Common/Common_model', 'common');
		$this->scripts = [
			base_url().'/assets/js/web/properties.js'
		];
		$this->limit = 3;
	}

	public function index($page = 1, $type = 0){
		$offset             = ($page-1)*$this->limit;
		$where 				= [];
		$data['js_array']   = $this->scripts;
		$data['page_title'] = 'Properties';

		if($this->session->userdata('logged_in')){
			$data['user'] = $this->session->userdata('logged_in');
		}

		if($type != 0){
			$where['sale_type'] = $type;
		}
		$where['status'] = 1;
		$data['properties'] = $this->common->ss_table_data('ss_properties',['images','property_name','property_location','selling_price','sale_type','pid', 'rent_deadline','availability'],$where, ['selling_price' =>'ASC'], ['per_page' => $this->limit, 'segment' => $offset]);

		$data['page_data']                = [];
		$data['page_data']['total_count'] = $this->common->ss_table_data('ss_properties',['count(*) as total'],$where)[0]['total'];
		$data['page_data']['pages']       = get_pages($data['page_data']['total_count'], $this->limit);

		$this->template->set_layout('layout/default_web');
		$this->template->initPartials('header','web_header');
		$this->template->initPartials('footer','web_footer');
		$this->template->buildPage('client/properties',$data);
	}

	public function load_table(){
		$data               = [];
		$where              = [];
		$search             = [];
		$page               = $this->input->post('page');
		$offset             = ($page-1)*$this->limit;
		$search_params      = $this->input->post('search_data');

		$data['page_label'] = 'perperty_list';
		if($search_params){
			if($search_params['property_name'] != ""){
				if((int)$search_params['property_name'] != 0){
					$where['selling_price <='] = (int)$search_params['property_name'];
				}else{
					$search['property_name'] = $search_params['property_name'];
					$search['property_location'] = $search_params['property_name'];
					// $search['sale_type'] = $search_params['property_type'];
				}
			}
		}

		$where['status'] = 1;
		$data['properties'] = $this->common->ss_table_data('ss_properties',['images','property_name','property_location','selling_price','sale_type','pid', 'rent_deadline','availability'],$where, ['selling_price' =>'ASC'], ['per_page' => $this->limit, 'segment' => $offset], $search);

		$this->load->view('client/client_page_template', $data);
	}
}
