<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Activate extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->scripts = [
			base_url().'/assets/js/web/activate.js',
			base_url().'assets/js/iziModal.min.js'
		];
		$this->load->model('Common/Common_model', 'common');
	}

	public function index(){
		$data = [];
		$data['page_title'] = 'Activate Premium Account';
		$data['js_array'] = $this->scripts;
		if($this->session->userdata('logged_in')){
			$data['user'] = $this->session->userdata('logged_in');
		}

		$this->template->set_layout('layout/default_web');
		$this->template->initPartials('header','web_header');
		$this->template->initPartials('footer','web_footer');
		$this->template->buildPage('client/activate',$data);
	}

	public function activate_account(){
		$code = $this->input->post('code');
		$email = $this->input->post('email');
		$pass = $this->input->post('pass');

		$retrieved_info = $this->common->ss_table_data('ss_accounts', [], ['email' => $email] );
		if($retrieved_info){
			$to_check_password = hash('sha256', $pass . $retrieved_info[0]['salt']);
			for($round = 0; $round < 65536; $round++){
				$to_check_password = hash('sha256', $to_check_password);
			}
			if($to_check_password == $retrieved_info[0]['password']){
				$user = $retrieved_info[0];

				if($user['premium'] == 0){
					$code_data = $this->common->ss_table_data('ss_code',[],['code' => $code]);
					if($code_data){
						if($code_data[0]['user'] == 0){
							$this->common->ss_update('ss_code',['user' => $user['user_id']],['id' => $code_data[0]['id']]);
							$this->common->ss_update('ss_accounts',['premium' => 1],['user_id' => $user['user_id']]);
							$status = 'success';
							$message = 'Account Activated Successfully';
						}else{
							$status = 'failed';
							$message = 'Code already been Used.';
						}
					}else{
						$status = 'failed';
						$message = 'Invalid Code Placed.';
					}
				}else{
					$status = 'failed';
					$message = 'User Account is already a Premium Member.';
				}

			}else{
				$status = 'failed';
				$message = 'Incorrect Password';
			}
		}else{
			$status = 'failed';
			$message = 'No Existing Account';
		}

		json_response($status,$message);
	}
}
