<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About_us extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->scripts = [
			base_url().'/assets/js/web/about.js'
		];
	}

	public function index(){
		$data = [];
		$data['page_title'] = 'About Us';
		$data['js_array'] = $this->scripts;
		if($this->session->userdata('logged_in')){
			$data['user'] = $this->session->userdata('logged_in');
		}

		$this->template->set_layout('layout/default_web');
		$this->template->initPartials('header','web_header');
		$this->template->initPartials('footer','web_footer');
		$this->template->buildPage('client/about',$data);
	}
}
