<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_us extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->scripts = [
			base_url().'/assets/js/web/contact_us.js',
			base_url().'assets/js/iziModal.min.js'
		];

	}

	public function index(){
		$data['js_array'] = $this->scripts;
		$data['page_title'] = 'Contact Us';
		if($this->session->userdata('logged_in')){
			$data['user'] = $this->session->userdata('logged_in');
		}
		$this->template->set_layout('layout/default_web');
		$this->template->initPartials('header','web_header');
		$this->template->initPartials('footer','web_footer');
		$this->template->buildPage('client/contact_us', $data);
	}

	public function send_message(){
		$details = $this->input->post('details');

		$headers =  'MIME-Version: 1.0' . "\r\n"; 
		$headers .= 'From: '.$details['name'].'<'.$details['email'].'>' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n"; 

		$subject = 'Seek4Space Contact Us || '.$details['subject'];
		$body = '<h3>Message</h3><br />'.$details['message'];
		// mail('utalk.jomarya@gmail.com',$subject,$body,$headers);
		
		json_response('success','email sent');
	}
}
