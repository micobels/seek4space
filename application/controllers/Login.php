<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->scripts = [
			base_url().'/assets/js/auth/authentication.js',
			base_url().'assets/js/iziModal.min.js'
		];
		$this->load->model('Common/Common_model', 'common');
	}

	public function index(){
		$data = [];
		$data['js_array'] = $this->scripts;
		$data['page_title'] = 'Login';
		$this->template->set_layout('layout/default_web');
		$this->template->initPartials('header','web_header');
		$this->template->initPartials('footer','web_footer');
		$this->template->buildPage('layout/login', $data);
	}

	public function process_login(){
		$params = $this->input->post('content');

		$retrieved_info = $this->common->ss_table_data('ss_accounts', [], ['email' => $params['email']] );

		if($retrieved_info){
			$to_check_password = hash('sha256', $params['password'] . $retrieved_info[0]['salt']);
			for($round = 0; $round < 65536; $round++){
				$to_check_password = hash('sha256', $to_check_password);
			}
			if($to_check_password == $retrieved_info[0]['password']){
				if($retrieved_info[0]['premium'] == 1){
					$this->session->set_userdata('logged_in',$retrieved_info[0]);
					json_response('success', 'User Found', []);
				}else{
					if(!strtotime($retrieved_info[0]['create_date']) < strtotime('-30 days')){
						$this->session->set_userdata('logged_in',$retrieved_info[0]);
						json_response('success', 'User Found', []);
					}else{
						json_response('failed', 'Free Trial Account Expired, Please Active into Premium Account.', []);
					}
				}
			}else{
				json_response('failed', 'Wrong Password', []);
			}
		}else{
			json_response('failed', 'No existing account', []);
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		json_response('success','logout');
	}
}
