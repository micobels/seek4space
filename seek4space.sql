/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100132
Source Host           : localhost:3306
Source Database       : seek4space

Target Server Type    : MYSQL
Target Server Version : 100132
File Encoding         : 65001

Date: 2019-02-06 21:41:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ss_accounts
-- ----------------------------
DROP TABLE IF EXISTS `ss_accounts`;
CREATE TABLE `ss_accounts` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0 - admin, 1 - buyer, 2 - seller',
  `email` varchar(255) NOT NULL,
  `password` varchar(500) NOT NULL,
  `salt` varchar(500) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `mname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `age` int(3) NOT NULL,
  `birth_date` varchar(255) NOT NULL,
  `birth_place` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `civil_status` tinyint(1) NOT NULL COMMENT '1 - single, 2 - married, 3 - widowed, 4 -divorced, 5 -separated',
  `gender` tinyint(1) NOT NULL COMMENT '1 - male, 2 - female',
  `nationality` varchar(255) NOT NULL,
  `citizenship` varchar(255) NOT NULL,
  `no_dependents` int(11) NOT NULL,
  `postal_code` varchar(255) NOT NULL,
  `contact_number` varchar(11) NOT NULL,
  `bank_account` varchar(255) NOT NULL,
  `about` varchar(255) NOT NULL,
  `fb_link` varchar(500) NOT NULL,
  `insta_link` varchar(500) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ss_accounts
-- ----------------------------
INSERT INTO `ss_accounts` VALUES ('17', '0', 'admin', 'cb6b6a5aeeb501f469a987dab477413bf0267e06dadb7cf95f3ef5b745620a47', '659b8cb813f8d186', 'admin', 'admin', 'admin', '0', '', '', '', '0', '0', '', '', '0', '', '', '', '', '', '');

-- ----------------------------
-- Table structure for ss_properties
-- ----------------------------
DROP TABLE IF EXISTS `ss_properties`;
CREATE TABLE `ss_properties` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `property_name` varchar(255) NOT NULL,
  `property_location` varchar(255) NOT NULL,
  `sale_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - not set, 1 - rent, 2 - transient, 3 - house and lot',
  `original_price` varchar(20) NOT NULL,
  `selling_price` varchar(20) NOT NULL,
  `images` text NOT NULL,
  `legal_docs` text NOT NULL,
  `creator` int(11) NOT NULL DEFAULT '0' COMMENT '0 - admin',
  `map` text NOT NULL,
  `description` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ss_properties
-- ----------------------------
INSERT INTO `ss_properties` VALUES ('33', 'Test Property Name 1', 'Test Property Address and Location 1', '1', '1500000', '2000000', '[\"assets\\/img\\/properties\\/Test-Property-Name-1\\/area\\/Test-Property-Name-10.jpeg\",\"assets\\/img\\/properties\\/Test-Property-Name-1\\/area\\/Test-Property-Name-11.jpeg\"]', '[\"assets\\/img\\/properties\\/Test-Property-Name-1\\/docs\\/Test-Property-Name-10.png\",\"assets\\/img\\/properties\\/Test-Property-Name-1\\/docs\\/Test-Property-Name-11.png\"]', '16', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3827.2135525495637!2d120.58167311421764!3d16.413977134467896!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3391a1701cdce349%3A0x50b22b23cd0ed82b!2sUTalk+Tutorial+Services!5e0!3m2!1sen!2sph!4v1547003055928\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', 'this is a property located in the heart of Baguio City.\n3 Bedrooms\n2 CR with hot and cold shower\nspacious Garage\nit even has its own garden and Swimming Pool', '1');
INSERT INTO `ss_properties` VALUES ('34', 'Property Test Name 2', 'Property Test Address and Location 2', '2', '5000000', '5500000', '[\"assets\\/img\\/properties\\/Property-Test-Name-2\\/area\\/Property-Test-Name-20.jpeg\",\"assets\\/img\\/properties\\/Property-Test-Name-2\\/area\\/Property-Test-Name-21.jpeg\"]', '[\"assets\\/img\\/properties\\/Property-Test-Name-2\\/docs\\/Property-Test-Name-20.jpeg\",\"assets\\/img\\/properties\\/Property-Test-Name-2\\/docs\\/Property-Test-Name-21.jpeg\"]', '16', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3827.2135525495637!2d120.58167311421764!3d16.413977134467896!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3391a16519f8072b%3A0x939af3d0552420e6!2sManhattan+Suites!5e0!3m2!1sen!2sph!4v1547003381149\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', 'this is a test property description', '1');
INSERT INTO `ss_properties` VALUES ('35', 'Property Test Name 3', 'Property Test Location and Address 3', '3', '8000000', '10000000', '[\"assets\\/img\\/properties\\/Property-Test-Name-3\\/area\\/Property-Test-Name-30.jpeg\",\"assets\\/img\\/properties\\/Property-Test-Name-3\\/area\\/Property-Test-Name-31.jpeg\",\"assets\\/img\\/properties\\/Property-Test-Name-3\\/area\\/Property-Test-Name-32.jpeg\",\"assets\\/img\\/properties\\/Property-Test-Name-3\\/area\\/Property-Test-Name-33.jpeg\",\"assets\\/img\\/properties\\/Property-Test-Name-3\\/area\\/Property-Test-Name-34.jpeg\"]', '[\"assets\\/img\\/properties\\/Property-Test-Name-3\\/docs\\/Property-Test-Name-30.jpeg\",\"assets\\/img\\/properties\\/Property-Test-Name-3\\/docs\\/Property-Test-Name-31.jpeg\"]', '16', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3827.2135525495637!2d120.58167311421764!3d16.413977134467896!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3391a1708769dfdd%3A0x9ce7793e36ea9e92!2sVilla+Caridad!5e0!3m2!1sen!2sph!4v1547003516750\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', 'this is a property located in the heart of Baguio City.\n3 Bedrooms\n2 CR with hot and cold shower\nspacious Garage\nit even has its own garden and Swimming Pool', '1');
INSERT INTO `ss_properties` VALUES ('36', 'Test Property Name 4', 'Test Property Address and Location 1', '1', '1500000', '2000000', '[\"assets\\/img\\/properties\\/Test-Property-Name-1\\/area\\/Test-Property-Name-10.jpeg\",\"assets\\/img\\/properties\\/Test-Property-Name-1\\/area\\/Test-Property-Name-11.jpeg\"]', '[\"assets\\/img\\/properties\\/Test-Property-Name-1\\/docs\\/Test-Property-Name-10.png\",\"assets\\/img\\/properties\\/Test-Property-Name-1\\/docs\\/Test-Property-Name-11.png\"]', '16', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3827.2135525495637!2d120.58167311421764!3d16.413977134467896!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3391a1701cdce349%3A0x50b22b23cd0ed82b!2sUTalk+Tutorial+Services!5e0!3m2!1sen!2sph!4v1547003055928\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', 'this is a property located in the heart of Baguio City.\n3 Bedrooms\n2 CR with hot and cold shower\nspacious Garage\nit even has its own garden and Swimming Pool', '1');
INSERT INTO `ss_properties` VALUES ('37', 'Property Test Name 5', 'Property Test Address and Location 2', '2', '5000000', '5500000', '[\"assets\\/img\\/properties\\/Property-Test-Name-2\\/area\\/Property-Test-Name-20.jpeg\",\"assets\\/img\\/properties\\/Property-Test-Name-2\\/area\\/Property-Test-Name-21.jpeg\"]', '[\"assets\\/img\\/properties\\/Property-Test-Name-2\\/docs\\/Property-Test-Name-20.jpeg\",\"assets\\/img\\/properties\\/Property-Test-Name-2\\/docs\\/Property-Test-Name-21.jpeg\"]', '0', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3827.2135525495637!2d120.58167311421764!3d16.413977134467896!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3391a16519f8072b%3A0x939af3d0552420e6!2sManhattan+Suites!5e0!3m2!1sen!2sph!4v1547003381149\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', 'this is a test property description', '1');
INSERT INTO `ss_properties` VALUES ('38', 'Property Test Name 6', 'Property Test Location and Address 3', '3', '8000000', '10000000', '[\"assets\\/img\\/properties\\/Property-Test-Name-3\\/area\\/Property-Test-Name-30.jpeg\",\"assets\\/img\\/properties\\/Property-Test-Name-3\\/area\\/Property-Test-Name-31.jpeg\",\"assets\\/img\\/properties\\/Property-Test-Name-3\\/area\\/Property-Test-Name-32.jpeg\",\"assets\\/img\\/properties\\/Property-Test-Name-3\\/area\\/Property-Test-Name-33.jpeg\",\"assets\\/img\\/properties\\/Property-Test-Name-3\\/area\\/Property-Test-Name-34.jpeg\"]', '[\"assets\\/img\\/properties\\/Property-Test-Name-3\\/docs\\/Property-Test-Name-30.jpeg\",\"assets\\/img\\/properties\\/Property-Test-Name-3\\/docs\\/Property-Test-Name-31.jpeg\"]', '16', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3827.2135525495637!2d120.58167311421764!3d16.413977134467896!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3391a1708769dfdd%3A0x9ce7793e36ea9e92!2sVilla+Caridad!5e0!3m2!1sen!2sph!4v1547003516750\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', 'this is a property located in the heart of Baguio City.\n3 Bedrooms\n2 CR with hot and cold shower\nspacious Garage\nit even has its own garden and Swimming Pool', '0');
INSERT INTO `ss_properties` VALUES ('40', 'test property mico', 'this is the test location of the property of mico', '3', '5000000', '10000000', '[\"assets\\/img\\/properties\\/test-property-mico\\/area\\/test-property-mico0.jpeg\",\"assets\\/img\\/properties\\/test-property-mico\\/area\\/test-property-mico1.jpeg\"]', '[\"assets\\/img\\/properties\\/test-property-mico\\/docs\\/test-property-mico0.jpeg\"]', '0', 'test map', 'this is the property description', '0');

-- ----------------------------
-- Table structure for ss_requests
-- ----------------------------
DROP TABLE IF EXISTS `ss_requests`;
CREATE TABLE `ss_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `property_id` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '0- pending,1-answered',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `receipt` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ss_requests
-- ----------------------------
INSERT INTO `ss_requests` VALUES ('7', '12', '33', '2', '2019-01-16 04:31:00', '[\"assets\\/img\\/receipt\\/33\\/33.12.1547609460.jpeg\"]');
INSERT INTO `ss_requests` VALUES ('8', '12', '34', '0', '2019-01-16 04:01:02', null);
INSERT INTO `ss_requests` VALUES ('9', '12', '35', '1', '2019-01-16 11:14:47', null);
INSERT INTO `ss_requests` VALUES ('10', '13', '36', '2', '2019-01-16 04:21:00', '[\"assets\\/img\\/receipt\\/36\\/36.13.1547608860.jpeg\"]');
INSERT INTO `ss_requests` VALUES ('11', '13', '39', '3', '2019-01-16 11:31:01', '[\"assets\\/img\\/receipt\\/39\\/39.13.1547608560.jpeg\"]');
INSERT INTO `ss_requests` VALUES ('12', '14', '37', '0', '2019-01-16 04:04:04', null);
