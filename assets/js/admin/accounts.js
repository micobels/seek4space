$(function () {
	$(document).ready(function(){
		$('.nav').find('.active').removeClass('active');
		$('.nav').find('li.accounts').addClass('active');
	})

	$("#account_details").iziModal({
		title: 'Account Details'
	});
	$("#success_modal").iziModal({
		headerColor: '#316931',
		width: 400,
		timeout: 3000,
		pauseOnHover: true,
		timeoutProgressbar: true,
		attached: 'bottom'			
	});

	$(document).on('click','.account_details',function(){
		id = $(this).attr('data-id');
		$("#account_details").iziModal('open');
		$.ajax({
			type  	: 'POST',
			url 	: base_url+"Admin/Accounts/Get_data",
			data 	: {'id':id},
			cache	: false,
			dataType : 'json',
			beforeSend: function(k){
				$("#account_details").iziModal('startLoading');
			},
			success : function(r){
				console.log(r);
				elem = '<tr><td style="font-weight: 1000;color: #000">Account ID</td><td>'+r.list.user_id+'</td></tr><tr><td style="font-weight: 1000;color: #000">Email Address</td><td>'+r.list.email+'</td></tr><tr><td style="font-weight: 1000;color: #000">Complete Name</td><td>'+r.list.fname +' '+r.list.mname+' '+r.list.lname+'</td></tr><tr><td style="font-weight: 1000;color: #000">Birth Date</td><td>'+r.list.birth_date+'</td></tr><tr><td style="font-weight: 1000;color: #000">Place of Birth</td><td>'+r.list.birth_place+'</td></tr><tr><td style="font-weight: 1000;color: #000">Address</td><td>'+r.list.address+'</td></tr><tr><td style="font-weight: 1000;color: #000">Civil Status</td><td>'
				if(r.list.civil_status == 2){
					elem +='Married'
				}else if(r.list.civil_status == 3){
					elem +='Widowed'
				}else if(r.list.civil_status == 4){
					elem +='Divorced'
				}else if(r.list.civil_status == 5){
					elem +='Separated'
				}else{
					elem +='Single'
				}
				elem +='</td></tr><tr><td style="font-weight: 1000;color: #000">Gender</td><td>'
				if(r.list.gender == 1){
					elem += 'Male'
				}else if(r.list.gender == 2){
					elem += 'Female'
				}else{
					elem += 'Others'
				}
				elem +='</td></tr><tr><td style="font-weight: 1000;color: #000">Nationality</td><td>'+r.list.nationality+'</td></tr><tr><td style="font-weight: 1000;color: #000">Postal Code</td><td>'+r.list.postal_code+'</td></tr><tr><td style="font-weight: 1000;color: #000">Contact Number</td><td>'+r.list.contact_number+'</td></tr><tr><td style="font-weight: 1000;color: #000">Bank Account</td><td>'+r.list.bank_account+'</td></tr><tr><td style="font-weight: 1000;color: #000">About</td><td>'+r.list.about+'</td></tr>'
				$("#account_details").find('tbody').html(elem);
				$("#account_details").iziModal('stopLoading');
			},
			error 	: function(e){
				console.log('error');
			}
		})
	})

	$(document).on('click','.delete_account',function(){
		id = $(this).attr('data-id');
		$.ajax({
			type  	: 'POST',
			url 	: base_url+"Admin/Accounts/Delete_account",
			data 	: {'id':id},
			cache	: false,
			dataType : 'json',
			success : function(r){
				$("#success_modal").iziModal('open');
				load_table(eval($('.current_page input').val()) || 1)
			},
			error 	: function(e){
				console.log('error');
			}
		})
	})

	$(document).on('click','.controls',function(){
		current_page = eval($('.current_page input').val()) || 1;
		last_page = eval($('.total_page').text());
		if($(this).hasClass('first_page')){
			current_page = 1;
		}else if($(this).hasClass('prev_page') && current_page > 1){
			current_page = current_page - 1;
		}else if($(this).hasClass('next_page') && current_page < last_page){
			current_page = current_page + 1;
		}else if($(this).hasClass('last_page')){
			current_page = last_page;
		}
		$('.current_page').find('input').val(current_page);
		load_table(current_page);
	})

	$(document).on('change','.current_page input',function(){
		current_page = eval($('.current_page input').val()) || 1;
		load_table(current_page);
	})

	function load_table(page){
		$.ajax({
			type: 'POST',
			url: base_url+'Admin/Accounts/load_table',
			data: {
				'page' : page
			},
			cache: false,
			dataType: 'html',
			beforeSend: function(f){
				$('#accounts_table').find('tbody').html('<tr><td colspan="6" style="text-align: center;color:#da8888;"><i class="fa fa-spin fa-spinner"></i></td></tr>');
			},
			success: function(r){
				$('#accounts_table').find('tbody').html(r);
			},
			error: function(e){
				console.log('error');
			}
		})
	}
})