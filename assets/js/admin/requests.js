$(function () {
	$(document).ready(function(){
		$('.nav').find('.active').removeClass('active');
		$('.nav').find('li.requests').addClass('active');
	})

	$(document).on('click','.email_modal',function(){
		$('#status_modal1').iziModal('open');
		id = $(this).attr('data-id');
		$('.change_email_status').attr('data-id',id);
	})

	$(document).on('click','.confirm_payment_modal',function(){
		$('#status_modal3').iziModal('open');
		id = $(this).attr('data-id');
		prop = $(this).attr('data-prop');
		$('.confirm_payment').attr('data-id',id);
		$('.confirm_payment').attr('data-prop',prop);
	})

	$(document).on('click','.change_email_status',function(){
		id = $(this).attr('data-id');
		change_status(id,1);
	})

	$(document).on('click','.confirm_payment',function(){
		id = $(this).attr('data-id');
		prop = $(this).attr('data-prop');
		change_status(id,3,prop);
	})

	function change_status(id,status,prop){
		status = status || 0;
		prop = prop || 0;
		$.ajax({
			type  	: 'POST',
			url 	: base_url+"Admin/Requests/change_status",
			data 	: {
				'id':id,
				'status' : status,
				'property' : prop
			},
			cache	: false,
			dataType : 'json',
			beforeSend: function(){
				if(status == 1){
					$("#status_modal1").iziModal('startLoading');
				}else{
					$("#status_modal3").iziModal('startLoading');
				}
			},
			success : function(r){
				if(status == 1){
					$("#status_modal1").iziModal('stopLoading');
					$("#status_modal1").iziModal('close');
				}else{
					$("#status_modal3").iziModal('stopLoading');
					$("#status_modal3").iziModal('close');
				}
				load_table(eval($('.current_page input').val()) || 1)
			},
			error 	: function(e){
				console.log('error');
			}
		})
	}

	$(document).on('click','.delete_requests',function(){
		var request_id = $(this).attr('data-id');
		$.ajax({
			type  	: 'POST',
			url 	: base_url+"Admin/Requests/delete_request",
			data 	: {'data_id' : request_id},
			cache	: false,
			dataType : 'json',
			success : function(r){
				$('#success_modal').iziModal('open');
				load_table(eval($('.current_page input').val()) || 1)
			},
			error 	: function(e){
				console.log('error');
			}
		})
	})

	$(document).on('click', '.mark_as_rented', function(){
		var prop_id = $(this).attr('data-id');
		var value = $(this).val();
		$.ajax({
			type  	: 'POST',
			url 	: base_url+"Admin/Requests/mask_rented",
			data 	: {'data_id' : prop_id, 'val' : value},
			cache	: false,
			dataType : 'json',
			success : function(r){
				$('#success_modal').iziModal('open');
				load_table(eval($('.current_page input').val()) || 1)
			},
			error 	: function(e){
				console.log('error');
			}
		})
	})

	$(document).on('click','.payment_notif',function(){
		$('#status_modal2').iziModal('open');
	})

	$("#status_modal1").iziModal({
		title: 'Client Contact Confirmation'
	});

	$("#status_modal2").iziModal({
		title: 'Notify Payment'
	});

	$("#status_modal3").iziModal({
		title: 'Payment Confirmation'
	});

	$("#success_modal").iziModal({
		headerColor: '#316931',
		width: 400,
		timeout: 3000,
		pauseOnHover: true,
		timeoutProgressbar: true,
		attached: 'bottom'			
	});

	$(document).on('click','.controls',function(){
		current_page = eval($('.current_page input').val()) || 1;
		last_page = eval($('.total_page').text());
		if($(this).hasClass('first_page')){
			current_page = 1;
		}else if($(this).hasClass('prev_page') && current_page > 1){
			current_page = current_page - 1;
		}else if($(this).hasClass('next_page') && current_page < last_page){
			current_page = current_page + 1;
		}else if($(this).hasClass('last_page')){
			current_page = last_page;
		}
		$('.current_page').find('input').val(current_page);
		load_table(current_page);
	})

	$(document).on('change','.current_page input',function(){
		current_page = eval($('.current_page input').val()) || 1;
		load_table(current_page);
	})

	function load_table(page){
		$.ajax({
			type: 'POST',
			url: base_url+'Admin/Requests/load_table',
			data: {
				'page' : page
			},
			cache: false,
			dataType: 'html',
			beforeSend: function(f){
				$('#requests_body').html('<tr><td colspan="6" style="text-align: center;color:#da8888;"><i class="fa fa-spin fa-spinner"></i></td></tr>');
			},
			success: function(r){
				$('#requests_body').html(r);
			},
			error: function(e){
				console.log('error');
			}
		})
	}
})