$(function () {

	$("#success_modal").iziModal({
		headerColor: '#316931',
		width: 400,
		timeout: 3000,
		pauseOnHover: true,
		timeoutProgressbar: true,
		attached: 'bottom'			
	});

	$("#notif_modal").iziModal({
		headerColor: '#b93b3b',
		width: 400,
		timeout: 3000,
		pauseOnHover: true,
		timeoutProgressbar: true,
		attached: 'bottom'			
	});

	$(document).on('click','.update_profile',function(e){
		e.preventDefault();
		console.log($('.gender').val());
		console.log($('.civil_status').val());
		if(check_update_fields()){
			var property_id = $('.user_id').val();
			data = {
				'id' : property_id,
				'email' : $('.email_address').val(), 
				'fname' : $('.fname').val(), 
				'mname' : $('.mname').val(), 
				'lname' : $('.lname').val(), 
				// 'age' : $('.age').val(), 
				'birth_date' : $('.birth_date').val(), 
				'birth_place' : $('.birth_place').val(), 
				'address' : $('.address').val(), 
				'civil_status' : $('.civil_status').val(), 
				'gender' : $('.gender').val(), 
				'nationality' : $('.nationality').val(), 
				// 'citizenship' : $('.citizenship').val(), 
				// 'dependents' : $('.dependents').val(), 
				'postal_code' : $('.postal_code').val(), 
				'mobile' : $('.mobile').val(), 
				'bank_account' : $('.bank_account').val(), 
				'about' : $('.about').val(), 
			};
			$.ajax({
				type  	: 'POST',
				url 	: base_url+"Admin/Profile/Update_profile",
				data 	: {'data': data},
				cache	: false,
				dataType : 'json',
				success : function(r){
					$('#success_modal').iziModal('open');
				},
				error 	: function(e){
					console.log('error');
				}
			})
		}else{
			$('#notif_modal').iziModal('open');
		}
	})

	function check_update_fields(){
		checker = true;
		$('.validate').each(function(){
			if($(this).val() == ''){
				checker = false;
				return false;
			}
		})

		return checker;
	}
})