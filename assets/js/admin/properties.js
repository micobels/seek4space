$(function () {
	$(document).ready(function(){
		$('.nav').find('.active').removeClass('active');
		$('.nav').find('li.properties').addClass('active');
	})

	$("#success_modal").iziModal({
		headerColor: '#316931',
		width: 400,
		timeout: 3000,
		pauseOnHover: true,
		timeoutProgressbar: true,
		attached: 'bottom'			
	});

	$("#notif_modal").iziModal({
		headerColor: '#b93b3b',
		width: 400,
		timeout: 3000,
		pauseOnHover: true,
		timeoutProgressbar: true,
		attached: 'bottom'			
	});

	$(document).on('click','.delete_property',function(){
		var property_id = $(this).attr('data-id');
		$.ajax({
			type  	: 'POST',
			url 	: base_url+"Admin/Properties/delete_property",
			data 	: {'data_id' : property_id},
			cache	: false,
			dataType : 'json',
			success : function(r){
				$('#success_modal').iziModal('open');
				load_table(eval($('.current_page input').val()) || 1)
			},
			error 	: function(e){
				console.log('error');
			}
		})
	})

	$(document).on('click','.house_img img',function(){
		$('.image').find('img').hide();
		new_img = $(this).attr('src');
		$('.image').find('img').attr('src',new_img).fadeIn();
	})

	$(document).on('click','.docu_img img',function(){
		$('.preview_document_cont').find('img').hide();
		new_img = $(this).attr('src');
		$('.preview_document_cont').find('img').attr('src',new_img).fadeIn();
	})

	$(document).on('click','.update_property',function(e){
		e.preventDefault();
		if(check_update_fields()){
			id = $('.property_id').val();
			form = {
				'status' : $('.up_status').val(),
				'name' : $('.up_name').val(),
				'address' : $('.up_address').val(),
				'orig' : $('.up_orig').val(),
				'selling' : $('.up_selling').val(),
				'id' : id
			}
			$.ajax({
				type  	: 'POST',
				url 	: base_url+"Admin/Manage/Update_property",
				data 	: form,
				cache	: false,
				dataType : 'json',
				success : function(r){
					$('#success_modal').iziModal('open');
				},
				error : function(e){
					console.log('error');
				}
			})
		}else{
			$('#notif_modal').iziModal('open');
		}
	})

	function check_update_fields(){
		checker = true;
		$('.validate').each(function(){
			if($(this).val() == '' || $(this).val() == 0){
				checker = false;
				return false;
			}
		})

		return checker;
	}

	$('.activate_prop').click(function(){
		id = $('.property_id').val();
		type = 1;
		active_property(type,id);
	})

	$('.deactivate_prop').click(function(){
		id = $('.property_id').val();
		type = 0;
		active_property(type,id);
	})

	function active_property(type,id){
		$.ajax({
			type  	: 'POST',
			url 	: base_url+"Admin/Manage/activate_property",
			data 	: {'type':type,'id':id},
			cache	: false,
			dataType : 'json',
			success : function(r){
				location.reload(false);
			},
			error : function(e){
				console.log('error');
			}
		})
	}

	$(document).on('click','.controls',function(){
		current_page = eval($('.current_page input').val()) || 1;
		last_page = eval($('.total_page').text());
		if($(this).hasClass('first_page')){
			current_page = 1;
		}else if($(this).hasClass('prev_page') && current_page > 1){
			current_page = current_page - 1;
		}else if($(this).hasClass('next_page') && current_page < last_page){
			current_page = current_page + 1;
		}else if($(this).hasClass('last_page')){
			current_page = last_page;
		}
		$('.current_page').find('input').val(current_page);
		load_table(current_page);
	})

	$(document).on('change','.current_page input',function(){
		current_page = eval($('.current_page input').val()) || 1;
		load_table(current_page);
	})

	function load_table(page){
		$.ajax({
			type: 'POST',
			url: base_url+'Admin/Properties/load_table',
			data: {
				'page' : page
			},
			cache: false,
			dataType: 'html',
			beforeSend: function(f){
				$('#property_list').html('<tr><td colspan="6" style="text-align: center;color:#da8888;"><i class="fa fa-spin fa-spinner"></i></td></tr>');
			},
			success: function(r){
				$('#property_list').html(r);
			},
			error: function(e){
				console.log('error');
			}
		})
	}
	if(document.getElementById('property_document_modal')){
		$("#property_document_modal").iziModal({
			title: 'Property Legal Documents'
		});
	}

	$('.open_documents').click(function(){
		$('#property_document_modal').iziModal('open');
		id = $('.property_id').val();
		$.ajax({
			type  	: 'POST',
			url 	: base_url+"Admin/Manage/get_legal_documents",
			data 	: {'id':id},
			cache	: false,
			dataType : 'json',
			beforeSend: function(){
				$('#property_document_modal').iziModal('startLoading');
			},
			success : function(r){
				elem = '<div class="preview_document_cont"><img src="'+base_url + r.list[0]+'"></div><ul class="list-inline" style="padding: 50px 50px 10px;">'
				for (var i = r.list.length - 1; i >= 0; i--) {
					elem += '<li class="docu_img"><img src="'+base_url + r.list[i]+'"></li>'
				}
    			elem += '</ul>'
    			$('#property_document_modal').find('.iziModal-content').html(elem);
    			$('#property_document_modal').iziModal('stopLoading');
			},
			error : function(e){
				console.log('error');
			}
		})
	})

	$(document).on('click', '.search_prop_info', function(){
		var search = {
			'keyword' : $('.search_prop_input').val(),
			'min' : $('.search_prop_input_min').val(),
			'max' : $('.search_prop_input_max').val(),
		}

		$.ajax({
			type  	: 'POST',
			url 	: base_url+"Admin/Properties/load_table",
			data 	: {
				'search':search,
				'page' : 1
			},
			cache	: false,
			dataType : 'html',
			success : function(r){
				$('#property_list').html(r);
			},
			error : function(e){
				console.log('error');
			}
		})
	})
})