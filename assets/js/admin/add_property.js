$(function () {
	// $(document).on('click', '.add_property_btn', function(){
	// 	var form    = $('.property_create_form');
	// 	var content = {
	// 		'name'       : form.find('.prop_name').val(),
	// 		'location'   : form.find('.prop_location').val(),
	// 		'type'       : form.find('.prop_sale_type').val(),
	// 		'orig_price' : form.find('.prop_orig_price').val(),
	// 		'sell_price' : form.find('.prop_sell_price').val(),
	// 	};


	// 	console.log(content);
	// 	$.ajax({
	// 		type  	: 'POST',
	// 		url 	: base_url+"admin/Add_property/save_property",
	// 		data 	: {'content' : content},
	// 		cache	: false,
	// 		dataType : 'json',
	// 		success : function(r){
	// 			console.log('success');
	// 		},
	// 		error 	: function(e){
	// 			console.log('error');
	// 		}
	// 	})
	// })

	$(document).ready(function(){
		$('.nav').find('.active').removeClass('active');
		$('.nav').find('li.properties').addClass('active');
	})

	$(document).on('change', '.prop_sale_type', function(){
		if($(this).val() == 1){
			$('.rental_info').show();
		}else{
			$('.rental_info').hide();
		}
	})

	function readURL(input,type) {
		var div_field = type == 1 ? $('.property_image_preview_section') : $('.legal_image_preview_section');
		if(input.files){
			div_field.empty();
			for (var i = input.files.length - 1; i >= 0; i--) {
				var reader = new FileReader();
				reader.onload = function (e) {
					div_field.append('<li><img src="'+e.target.result+'"></li>');
				}
				reader.readAsDataURL(input.files[i]);
			}
		}
	}

	$('#property_images').on('change', function () {
		readURL(this,1);	
	});
	$('#legal_document').on('change', function () {
		readURL(this,2);	
	});

	$(document).on('submit', '#add_property_serialize', function(e){
		e.preventDefault();
		var form    = $('.property_create_form');
		if(check_fields(form)){
			var content = {
				'name'       : form.find('.prop_name').val(),
				'location'   : form.find('.prop_location').val(),
				'type'       : form.find('.prop_sale_type').val(),
				// 'orig_price' : form.find('.prop_orig_price').val(),
				'sell_price' : form.find('.prop_sell_price').val(),
				'embed' 	 : form.find('.embed_map').val(),
				'about' 	 : form.find('.desc_drop').val() + " : " +form.find('.desc_input').val(),
				'rent_date'  : form.find('.rent_date').val(),
			};

			form_data = new FormData(e.target);
			for ( var key in content ) {
			    form_data.append(key, content[key]);
			}
			console.log(content)
			$.ajax({
				type  	: 'POST',
				url 	: base_url+"Admin/Add_property/save_property",
				data 	: form_data,
				cache	: false,
				dataType : 'json',
				processData: false,
				contentType: false,
				success : function(r){
					$('#success_modal').iziModal('open');
					reset_fields();
				},
				error 	: function(e){
					console.log('error');
				}
			})
		}else{
			$('#notif_modal').iziModal('open');
		}
	})

	function check_fields(elem){
		checker = true;
		elem.find('.validate').each(function(){
			if($(this).is(':visible')) {
				if($(this).val() == '' || $(this).val() == 0){
					checker = false;
					return false;
				}
			}
		})
		return checker;
	}

	function reset_fields(){
		$('.validate').each(function(){
			$(this).val('');
		})
		$('.prop_sale_type').val(0);
		$('textarea').val('');
		$('.property_image_preview_section').empty();
		$('.legal_image_preview_section').empty();
	}

	$("#success_modal").iziModal({
		headerColor: '#316931',
		width: 400,
		timeout: 3000,
		pauseOnHover: true,
		timeoutProgressbar: true,
		attached: 'bottom'			
	});
	$("#notif_modal").iziModal({
		headerColor: '#b93b3b',
		width: 400,
		timeout: 3000,
		pauseOnHover: true,
		timeoutProgressbar: true,
		attached: 'bottom'			
	});
}) // end of $function