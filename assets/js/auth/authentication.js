$(function () {
	$(document).on('click', '.proceed_register_btn', function (){
		var form = $('#registration_form');
		var address = form.find('.house_num').val() + ' ' + form.find('.house_street').val() + ' ' + form.find('.house_city').val() + ' ' + form.find('.house_prov').val();

		var content = {
			'utype' : form.find('.reg_utype').val(),
			'email' : form.find('.reg_email').val(),
			'fname' : form.find('.reg_fname').val(),
			'mname' : form.find('.reg_mname').val(),
			'lname' : form.find('.reg_lname').val(),
			// 'age' : form.find('.reg_age').val(),
			'bday' : form.find('.reg_birth_date').val(),
			'bplace' : form.find('.reg_birth_place').val(),
			'address' : address,
			'civil_stat' : form.find('.reg_civil_stat').val(),
			'gender' : form.find('.reg_gender').val(),
			'nationality' : form.find('.reg_nationality').val(),
			// 'citizenship' : form.find('.reg_citizenship').val(),
			// 'dependents' : form.find('.reg_dependents').val(),
			'postal_code' : form.find('.reg_postal').val(),
			'contact_no' : form.find('.reg_contact_no').val(),
			'bank_account' : form.find('.reg_bank_account').val(),
			'about' : form.find('.reg_about').val(),
			'facebook' : form.find('.reg_facebook').val(),
			'intagram' : form.find('.reg_instagram').val(),
			'password' : form.find('.reg_password').val(),
		};

		proceed = true;

		$('.validate').each(function(){
			if($(this).val() == ''){
				proceed = false;
				$('#notif_modal').iziModal('setSubtitle','Please Fill up all the Missing Fields');
				$('#notif_modal').iziModal('open');
				return false;
			}
		})

		if(content.password != form.find('.reg_confirm_password').val()){
			$('#notif_modal').iziModal('setSubtitle','Password Does not Match');
			$('#notif_modal').iziModal('open');
			return false;
		}

		console.log(content);
		if(proceed){
			$.ajax({
				type  	: 'POST',
				url 	: base_url+"Register/process_registration",
				data 	: {"content" : content},
				cache	: false,
				dataType : 'json',
				success : function(r){
					console.log('success');
					window.location.href=base_url + 'Login';
				},
				error 	: function(e){
					console.log('error');
				}
			})
		}
	});

	$(document).on('click', '.login_btn', function (){
		var form = $('.login_form');
		var login_data = {
			'email' : form.find('.login_email').val(),
			'password' : form.find('.login_password').val()
		};

		if(!login_data.email || !login_data.password){
			console.log("incomplete data");
			$('#notif_modal').iziModal('setSubtitle','incomplete data');
			$('#notif_modal').iziModal('open');
			return false;
		}

		$.ajax({
			type  	: 'POST',
			url 	: base_url+"Login/process_login",
			data 	: {"content" : login_data},
			cache	: false,
			dataType : 'json',
			success : function(r){
				if(r.status == 'success'){
					window.location.href=base_url;
				}else{
					$('#notif_modal').iziModal('setSubtitle',r.message);
					$('#notif_modal').iziModal('open');
				}
			},
			error 	: function(e){
				console.log('error');
			}
		})
	})
	if($("#notif_modal").length){
		$("#notif_modal").iziModal({
			headerColor: '#b93b3b',
			width: 400,
			attached: 'bottom'			
		});
	}
}) // end of main function