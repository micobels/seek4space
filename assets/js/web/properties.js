$(function () {
	$(document).ready(function(){
		$('.nav').find('.active').removeClass('active');
		$('.nav').find('li').eq(2).addClass('active');
	})

	$('.redirect_request').click(function(){
		id = $(this).attr('data-id');
		$.ajax({
			type  	: 'POST',
			url 	: base_url+"Client/Property/set_request",
			data 	: {'id':id},
			cache	: false,
			dataType : 'json',
			success : function(r){
				console.log(r.status);
				if(r.status == 'success'){
					window.location.href= base_url + 'Client/Property/request/' + id;
				}
			},
			error : function(e){
				console.log('error');
			}
		})
	})

	if($("#property_payment").length){
		$("#property_payment").iziModal({
			title: 'Property Payment Details',
			headerColor: '#105a9c'
		});
	}

	if($("#login_modal").length){
		$("#login_modal").iziModal({
			title: 'Notification',
			headerColor: '#105a9c'
		});
	}

	$('.payment_btn').click(function(){
		$("#property_payment").iziModal('open');
	})

	$('.login_first').click(function(){
		$("#login_modal").iziModal('open');
	})

	// $('.export_documents').click(function(){
	// 	$.ajax({
	// 		type  	: 'POST',
	// 		url 	: base_url+"Client/Property/download_folder",
	// 		cache	: false,
	// 		dataType : 'json',
	// 		success : function(r){
	// 			console.log('success');
	// 		},
	// 		error 	: function(e){
	// 			console.log('error');
	// 		}
	// 	})
	// })

	function readURL(input) {
		if(input.files){
			for (var i = input.files.length - 1; i >= 0; i--) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$('.reciept_image_container').append('<img src="'+e.target.result+'">');
				}
				reader.readAsDataURL(input.files[i]);
			}
		}
	}

	$('#property_receipt').on('change', function () {
		readURL(this);	
		$('.notif_upload').hide();
	});

	$(document).on('submit', '#property_receipt_serialize', function(e){
		e.preventDefault();

		if($('#property_receipt').val()){
			form_data = new FormData(e.target);
			form_data.append('id', $('.submit_receipt').attr('data-id'));

			$.ajax({
				type  	: 'POST',
				url 	: base_url+"Client/Property/upload_payment",
				data 	: form_data,
				cache	: false,
				dataType : 'json',
				processData: false,
				contentType: false,
				beforeSend: function(k){
					$("#property_payment").iziModal('startLoading');
				},
				success : function(r){
					$("#property_payment").iziModal('stopLoading');
					$('.initial_view').hide();
					$('.success_view').fadeIn();
				},
				error 	: function(e){
					console.log('error');
				}
			})
		}else{
			$('.notif_upload').fadeIn();
		}
	});

	$(document).on('click', '.page_control', function(){
		var page       = $('.client_prop_page').find('.active_page').val();
		var total_page = $('.total_page').text();
		if($(this).hasClass('first_page')){
			page = 1;
		}
		if($(this).hasClass('prev_page')){
			page = eval(page) - 1;
		}
		if($(this).hasClass('next_page')){
			page = eval(page) + 1;
		}
		if($(this).hasClass('last_page')){
			page = total_page;
		}
		
		if((page <= total_page) && (page > 0)){
			$('.client_prop_page').find('.active_page').val(page)
			$.ajax({
				type  	: 'POST',
				url 	: base_url+"Client/Properties/load_table",
				data 	: {page:page},
				dataType : 'html',
				success : function(r){
					$('.properties_inner').html(r);
				},
				error 	: function(e){
					console.log('error');
				}
			})
		}
	});

	$(document).on('click', '.search_client_prop', function(){
		var form = $('.client_property_search');
		search_data = {
			'property_name' : form.find('.main_search').val(),
			// 'property_loc' : form.find('.prop_location').val(),
			// 'property_type' : form.find('.prop_type').val(),
			// 'property_min_price' : form.find('.prop_min_price').val(),
			// 'property_max_price' : form.find('.prop_max_price').val(),
		};

		console.log(search_data);
		$.ajax({
			type  	: 'POST',
			url 	: base_url+"Client/Properties/load_table",
			data 	: {
				page : 1,
				search_data : search_data
			},
			dataType : 'html',
			success : function(r){
				$('.properties_inner').html(r);
			},
			error 	: function(e){
				console.log('error');
			}
		})
	});
})