$(function () {
	$('.activate_premium').click(function(){
		code = $('.activation_code').val();
		pass = $('.login_password').val();
		email = $('.login_email').val();

		if(code != '' && pass != '' && email != ''){
			$('.notif_message').fadeOut();
			$.ajax({
				type  	: 'POST',
				url 	: base_url+"Client/Activate/activate_account",
				data 	: {'code':code,'pass':pass,'email':email},
				cache	: false,
				dataType : 'json',
				success : function(r){
					console.log(r);
					if(r.status == 'success'){
						$('#success_modal').iziModal('setSubtitle',r.message);
						$('#success_modal').iziModal('open');
						setTimeout(function(){
							window.location.href= base_url;
						},3000)
					}else{
						$('#notif_modal').iziModal('setSubtitle',r.message);
						$('#notif_modal').iziModal('open');
					}
				},
				error 	: function(e){
					console.log('error');
				}
			})
		}else{
			$('#notif_modal').iziModal('open');
		}
	})

	if($("#notif_modal").length){
		$("#notif_modal").iziModal({
			headerColor: '#b93b3b',
			width: 400,
			attached: 'bottom'			
		});
	}
	if($("#success_modal").length){
		$("#success_modal").iziModal({
			headerColor: '#316931',
			width: 400,
			timeout: 3000,
			pauseOnHover: true,
			timeoutProgressbar: true,
			attached: 'bottom'					
		});
	}
});