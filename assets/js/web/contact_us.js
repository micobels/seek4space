$(function () {
	$(document).ready(function(){
		$('.nav').find('.active').removeClass('active');
		$('.nav').find('li').eq(3).addClass('active');
	})

	$("#success_modal").iziModal({
		headerColor: '#316931',
		width: 400,
		timeout: 3000,
		pauseOnHover: true,
		timeoutProgressbar: true,
		attached: 'bottom'			
	});

	$('.submit_message').click(function(e){
		e.preventDefault();
		$("#success_modal").iziModal('open');
		contact = {
			'name' : $('.contact_name').val(),
			'email' : $('.contact_email').val(),
			'subject' : $('.contact_subject').val(),
			'message' : $('.contact_message').val()
		};

		$.ajax({
			type  	: 'POST',
			url 	: base_url+"Client/Contact_us/send_message",
			data 	: {'details':contact},
			cache	: false,
			dataType : 'json',
			beforeSend: function(){
				$("#success_modal").iziModal('startLoading');
			},
			success : function(r){
				$("#success_modal").iziModal('stopLoading');
				clear_fields();
			},
			error 	: function(e){
				console.log('error');
			}
		})
	})

	function clear_fields(){
		$('.validate').each(function(){
			$(this).val('');
		})
	}
})